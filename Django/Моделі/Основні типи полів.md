#model 
## CharField
Поле `CharField` в моделях Django використову
ється для зберігання рядкових даних з обмеженою довжиною. Це одне з найпоширеніших полів у Django, яке дозволяє зберігати текстову інформацію, таку як назви, імена, опис тощо.
[CharField(Documentation Django)](https://docs.djangoproject.com/en/5.0/ref/models/fields/#charfield)
### Основні атрибути

#### 1. `max_length`
Обов'язковий атрибут, який визначає максимальну довжину рядка в символах. Це необхідно для створення відповідної колонки у базі даних.

```python
class Book(models.Model):
    title = models.CharField(max_length=200)
```

#### 2. `null`
Визначає, чи може поле містити `NULL` значення в базі даних. Значення за замовчуванням – `False`.

```python
class Book(models.Model):
    title = models.CharField(max_length=200, null=True)
```

#### 3. `blank`
Визначає, чи може поле бути порожнім у формах. Значення за замовчуванням – `False`.

```python
class Book(models.Model):
    title = models.CharField(max_length=200, blank=True)
```

#### 4. `default`
Задає значення за замовчуванням для поля.

```python
class Book(models.Model):
    title = models.CharField(max_length=200, default='Untitled')
```

#### 5. `choices`
Задає набір допустимих значень для поля у вигляді кортежів. Кожен кортеж містить пару (значення, читабельне значення).

```python
class Book(models.Model):
    GENRE_CHOICES = [
        ('FIC', 'Fiction'),
        ('NF', 'Non-Fiction'),
        ('MYS', 'Mystery'),
        ('SCI', 'Science Fiction'),
    ]
    genre = models.CharField(max_length=3, choices=GENRE_CHOICES)
```

#### 6. `unique`
Визначає, чи повинні значення поля бути унікальними у всій таблиці. Значення за замовчуванням – `False`.

```python
class Book(models.Model):
    title = models.CharField(max_length=200, unique=True)
```

#### 7. `validators`
Дозволяє задати список функцій-валідаторів, які будуть використовуватися для перевірки даних, що вводяться в поле.

```python
from django.core.validators import MinLengthValidator

class Book(models.Model):
    title = models.CharField(max_length=200, validators=[MinLengthValidator(10)])
```

### Приклад використання

Ось повний приклад моделі, що використовує `CharField` з різними атрибутами:

```python
from django.db import models
from django.core.validators import MinLengthValidator

class Author(models.Model):
    name = models.CharField(max_length=100)

    def __str__(self):
        return self.name

class Book(models.Model):
    title = models.CharField(max_length=200, unique=True, validators=[MinLengthValidator(5)])
    author = models.ForeignKey(Author, on_delete=models.CASCADE)
    genre = models.CharField(max_length=3, choices=[
        ('FIC', 'Fiction'),
        ('NF', 'Non-Fiction'),
        ('MYS', 'Mystery'),
        ('SCI', 'Science Fiction'),
    ])
    summary = models.CharField(max_length=500, blank=True, null=True)

    def __str__(self):
        return self.title
```

У цьому прикладі:
- `title` має максимальну довжину 200 символів, унікальне значення та валідатор мінімальної довжини 5 символів.
- `genre` використовує `choices` для обмеження значень певним набором.
- `summary` дозволяє порожні значення та `NULL`.

### Висновок

Поле `CharField` в моделях Django є гнучким інструментом для зберігання текстових даних з обмеженою довжиною. Воно дозволяє налаштовувати багато аспектів поведінки поля, таких як максимальна довжина, унікальність, набір допустимих значень та валідацію. Це робить його незамінним при роботі з рядковими даними в базах даних.

## TextField
Поле `TextField` в моделях Django використовується для зберігання великих обсягів текстових даних. На відміну від `CharField`, `TextField` не має обмеження на кількість символів, що робить його ідеальним для зберігання довгих текстів, таких як описи, коментарі, статті тощо.
[TextField(Documentation Django)](https://docs.djangoproject.com/en/5.0/ref/models/fields/#textfield)
### Основні атрибути

#### 1. `null`
Визначає, чи може поле містити значення `NULL` у базі даних. Значення за замовчуванням – `False`.

```python
class Article(models.Model):
    content = models.TextField(null=True)
```

#### 2. `blank`
Визначає, чи може поле бути порожнім у формах. Значення за замовчуванням – `False`.

```python
class Article(models.Model):
    content = models.TextField(blank=True)
```

#### 3. `default`
Задає значення за замовчуванням для поля.

```python
class Article(models.Model):
    content = models.TextField(default='This is the default content.')
```

#### 4. `choices`
Задає набір допустимих значень для поля у вигляді кортежів. Кожен кортеж містить пару (значення, читабельне значення). Хоча цей атрибут рідко використовується з `TextField`, він все ж підтримується.

```python
class Article(models.Model):
    STATUS_CHOICES = [
        ('draft', 'Draft'),
        ('published', 'Published'),
        ('archived', 'Archived'),
    ]
    status = models.TextField(choices=STATUS_CHOICES)
```

#### 5. `validators`
Дозволяє задати список функцій-валідаторів, які будуть використовуватися для перевірки даних, що вводяться в поле.

```python
from django.core.validators import MinLengthValidator

class Article(models.Model):
    content = models.TextField(validators=[MinLengthValidator(10)])
```

#### 6. `help_text`
Задає допоміжний текст, який буде відображатися у формах.

```python
class Article(models.Model):
    content = models.TextField(help_text='Enter the content of the article here.')
```

### Приклад використання

Ось повний приклад моделі, що використовує `TextField` з різними атрибутами:

```python
from django.db import models
from django.core.validators import MinLengthValidator

class Article(models.Model):
    title = models.CharField(max_length=200)
    content = models.TextField(
        blank=True,
        null=True,
        validators=[MinLengthValidator(10)],
        help_text='Enter the content of the article here.'
    )
    status = models.TextField(choices=[
        ('draft', 'Draft'),
        ('published', 'Published'),
        ('archived', 'Archived'),
    ], default='draft')

    def __str__(self):
        return self.title
```

У цьому прикладі:
- `title` є коротким текстовим полем з обмеженням у 200 символів.
- `content` є текстовим полем, яке може бути порожнім або містити значення `NULL`, має валідатор мінімальної довжини 10 символів та допоміжний текст.
- `status` використовує `choices` для обмеження значень певним набором і має значення за замовчуванням `draft`.

### Висновок

Поле `TextField` в моделях Django є потужним інструментом для зберігання великих обсягів текстових даних. Воно підтримує багато налаштувань, що дозволяє гнучко керувати поведінкою поля, включаючи налаштування значень за замовчуванням, валідацію, допоміжний текст та допустимі значення. Це робить його ідеальним для зберігання довгих текстів у додатках Django.

## IntegerField
Поле `IntegerField` в моделях Django використовується для зберігання цілих чисел. Це поле корисне для зберігання числових даних, таких як кількість товарів, рейтинг, вік тощо.
[IntegerField(Django Documentaion)](https://docs.djangoproject.com/en/5.0/ref/models/fields/#integerfield)
### Основні атрибути

#### 1. `null`
Визначає, чи може поле містити значення `NULL` у базі даних. Значення за замовчуванням – `False`.

```python
class Product(models.Model):
    quantity = models.IntegerField(null=True)
```

#### 2. `blank`
Визначає, чи може поле бути порожнім у формах. Значення за замовчуванням – `False`.

```python
class Product(models.Model):
    quantity = models.IntegerField(blank=True)
```

#### 3. `default`
Задає значення за замовчуванням для поля.

```python
class Product(models.Model):
    quantity = models.IntegerField(default=0)
```

#### 4. `choices`
Задає набір допустимих значень для поля у вигляді кортежів. Кожен кортеж містить пару (значення, читабельне значення).

```python
class Product(models.Model):
    STATUS_CHOICES = [
        (0, 'Unavailable'),
        (1, 'In Stock'),
        (2, 'Limited Stock'),
    ]
    status = models.IntegerField(choices=STATUS_CHOICES)
```

#### 5. `validators`
Дозволяє задати список функцій-валідаторів, які будуть використовуватися для перевірки даних, що вводяться в поле.

```python
from django.core.validators import MinValueValidator, MaxValueValidator

class Product(models.Model):
    quantity = models.IntegerField(validators=[MinValueValidator(0), MaxValueValidator(100)])
```

#### 6. `help_text`
Задає допоміжний текст, який буде відображатися у формах.

```python
class Product(models.Model):
    quantity = models.IntegerField(help_text='Enter the quantity of the product in stock.')
```

#### 7. `unique`
Визначає, чи повинні значення поля бути унікальними у всій таблиці. Значення за замовчуванням – `False`.

```python
class Product(models.Model):
    sku = models.IntegerField(unique=True)
```

### Приклад використання

Ось повний приклад моделі, що використовує `IntegerField` з різними атрибутами:

```python
from django.db import models
from django.core.validators import MinValueValidator, MaxValueValidator

class Product(models.Model):
    name = models.CharField(max_length=200)
    quantity = models.IntegerField(
        null=True,
        blank=True,
        default=0,
        validators=[MinValueValidator(0), MaxValueValidator(1000)],
        help_text='Enter the quantity of the product in stock.'
    )
    status = models.IntegerField(choices=[
        (0, 'Unavailable'),
        (1, 'In Stock'),
        (2, 'Limited Stock'),
    ], default=1)

    def __str__(self):
        return self.name
```

У цьому прикладі:
- `name` є коротким текстовим полем для зберігання назви продукту.
- `quantity` є цілим числом, яке може бути порожнім або містити значення `NULL`, має значення за замовчуванням 0, валідатори мінімальної та максимальної кількості, а також допоміжний текст.
- `status` використовує `choices` для обмеження значень певним набором і має значення за замовчуванням `1`.

### Висновок

Поле `IntegerField` в моделях Django є основним інструментом для зберігання цілих чисел. Воно підтримує різноманітні налаштування, такі як значення за замовчуванням, валідація, допоміжний текст, унікальність та допустимі значення. Це робить його дуже гнучким та корисним для зберігання числових даних у додатках Django.

## FloatField
Поле `FloatField` в моделях Django використовується для зберігання чисел з плаваючою комою. Це поле підходить для зберігання чисел, які потребують дробових значень, таких як ціни, виміри, ваги тощо.
[FloatField(Documentation Django)](https://docs.djangoproject.com/en/5.0/ref/models/fields/#floatfield)
### Основні атрибути

#### 1. `null`
Визначає, чи може поле містити значення `NULL` у базі даних. Значення за замовчуванням – `False`.

```python
class Product(models.Model):
    price = models.FloatField(null=True)
```

#### 2. `blank`
Визначає, чи може поле бути порожнім у формах. Значення за замовчуванням – `False`.

```python
class Product(models.Model):
    price = models.FloatField(blank=True)
```

#### 3. `default`
Задає значення за замовчуванням для поля.

```python
class Product(models.Model):
    price = models.FloatField(default=0.0)
```

#### 4. `choices`
Задає набір допустимих значень для поля у вигляді кортежів. Кожен кортеж містить пару (значення, читабельне значення).

```python
class Product(models.Model):
    QUALITY_CHOICES = [
        (1.0, 'Poor'),
        (2.0, 'Average'),
        (3.0, 'Good'),
        (4.0, 'Very Good'),
        (5.0, 'Excellent'),
    ]
    quality = models.FloatField(choices=QUALITY_CHOICES)
```

#### 5. `validators`
Дозволяє задати список функцій-валідаторів, які будуть використовуватися для перевірки даних, що вводяться в поле.

```python
from django.core.validators import MinValueValidator, MaxValueValidator

class Product(models.Model):
    price = models.FloatField(validators=[MinValueValidator(0.0), MaxValueValidator(10000.0)])
```

#### 6. `help_text`
Задає допоміжний текст, який буде відображатися у формах.

```python
class Product(models.Model):
    price = models.FloatField(help_text='Enter the price of the product.')
```

#### 7. `unique`
Визначає, чи повинні значення поля бути унікальними у всій таблиці. Значення за замовчуванням – `False`.

```python
class Product(models.Model):
    price = models.FloatField(unique=True)
```

### Приклад використання

Ось повний приклад моделі, що використовує `FloatField` з різними атрибутами:

```python
from django.db import models
from django.core.validators import MinValueValidator, MaxValueValidator

class Product(models.Model):
    name = models.CharField(max_length=200)
    price = models.FloatField(
        null=True,
        blank=True,
        default=0.0,
        validators=[MinValueValidator(0.0), MaxValueValidator(10000.0)],
        help_text='Enter the price of the product.'
    )
    quality = models.FloatField(choices=[
        (1.0, 'Poor'),
        (2.0, 'Average'),
        (3.0, 'Good'),
        (4.0, 'Very Good'),
        (5.0, 'Excellent'),
    ], default=3.0)

    def __str__(self):
        return self.name
```

У цьому прикладі:
- `name` є коротким текстовим полем для зберігання назви продукту.
- `price` є числом з плаваючою комою, яке може бути порожнім або містити значення `NULL`, має значення за замовчуванням 0.0, валідатори мінімальної та максимальної вартості, а також допоміжний текст.
- `quality` використовує `choices` для обмеження значень певним набором і має значення за замовчуванням `3.0`.

### Висновок

Поле `FloatField` в моделях Django є основним інструментом для зберігання чисел з плаваючою комою. Воно підтримує різноманітні налаштування, такі як значення за замовчуванням, валідація, допоміжний текст, унікальність та допустимі значення. Це робить його дуже гнучким та корисним для зберігання дробових числових даних у додатках Django.

## BooleanField
Поле `BooleanField` в моделях Django використовується для зберігання логічних значень (True або False). Це поле часто застосовується для позначення станів або прапорців, таких як активація, публікація, доступність тощо.
[Model field reference | Django documentation | Django (djangoproject.com)](https://docs.djangoproject.com/en/5.0/ref/models/fields/#booleanfield)
### Основні атрибути

#### 1. `null`
Поле `BooleanField` підтримує значення `NULL`. 

```python
class ExampleModel(models.Model):
    is_active = models.BooleanField(null=True)  
```

#### 2. `default`
Задає значення за замовчуванням для поля. Це значення буде використовуватися, якщо при створенні об'єкта не було передано значення для цього поля.

```python
class ExampleModel(models.Model):
    is_active = models.BooleanField(default=True)
```

#### 3. `choices`
Поле `BooleanField` не використовує атрибут `choices`, оскільки можливі значення завжди обмежені двома: True і False.

#### 4. `blank`
Визначає, чи може поле бути порожнім у формах. Значення за замовчуванням – `False`. Зазвичай використовується разом із `default`.

```python
class ExampleModel(models.Model):
    is_active = models.BooleanField(blank=True, default=False)
```

#### 5. `validators`
Дозволяє задати список функцій-валідаторів, які будуть використовуватися для перевірки даних, що вводяться в поле.

```python
from django.core.exceptions import ValidationError

def validate_boolean(value):
    if not isinstance(value, bool):
        raise ValidationError(f'{value} is not a boolean value')

class ExampleModel(models.Model):
    is_active = models.BooleanField(validators=[validate_boolean])
```

#### 6. `help_text`
Задає допоміжний текст, який буде відображатися у формах. Це корисно для надання додаткової інформації користувачам щодо того, як використовувати це поле.

```python
class ExampleModel(models.Model):
    is_active = models.BooleanField(help_text='Designates whether this instance should be treated as active.')
```

### Приклад використання

Ось повний приклад моделі, що використовує `BooleanField` з різними атрибутами:

```python
from django.db import models

class User(models.Model):
    username = models.CharField(max_length=150, unique=True)
    email = models.EmailField(unique=True)
    is_active = models.BooleanField(default=True, help_text='Designates whether this user should be treated as active.')
    is_staff = models.BooleanField(default=False, help_text='Designates whether the user can log into this admin site.')

    def __str__(self):
        return self.username
```

У цьому прикладі:
- `username` є коротким текстовим полем для зберігання імені користувача.
- `email` є полем для зберігання електронної пошти користувача.
- `is_active` є логічним полем, яке визначає, чи є користувач активним. Воно має значення за замовчуванням `True` та допоміжний текст.
- `is_staff` є логічним полем, яке визначає, чи є користувач членом персоналу, з доступом до адміністративної частини сайту. Воно має значення за замовчуванням `False` та допоміжний текст.

### Висновок

Поле `BooleanField` в моделях Django є зручним інструментом для зберігання логічних значень. Воно підтримує кілька важливих атрибутів, таких як значення за замовчуванням, допоміжний текст і валідацію. Це робить його корисним для реалізації різних станів і прапорців у додатках Django.
## ForeignKey
Поле `ForeignKey` в моделях Django використовується для створення зв'язків "багато-до-одного" між моделями. Це означає, що кожен екземпляр однієї моделі може посилатися на один екземпляр іншої моделі, але один екземпляр іншої моделі може мати багато посилань з першої моделі.
[Model field reference | Django documentation | Django (djangoproject.com)](https://docs.djangoproject.com/en/5.0/ref/models/fields/#foreignkey)

### Синтаксис

```python
from django.db import models

class Author(models.Model):
    name = models.CharField(max_length=100)

class Book(models.Model):
    title = models.CharField(max_length=200)
    author = models.ForeignKey(Author, on_delete=models.CASCADE)
```

### Атрибути

1. **`to`**:
   - Обов'язковий аргумент, який вказує на модель, до якої створюється зв'язок. Це може бути або клас моделі, або назва моделі у вигляді рядка.
   - Приклад: `author = models.ForeignKey(Author, on_delete=models.CASCADE)`

2. **`on_delete`**:
   - Обов'язковий аргумент, який визначає поведінку при видаленні пов'язаної моделі. Значення можуть бути:
     - `models.CASCADE`: Видалити об'єкт, на який посилається це поле.
     - `models.PROTECT`: Запобігти видаленню об'єкта, на який посилається це поле, і підняти помилку `ProtectedError`.
     - `models.SET_NULL`: Встановити поле у значення `NULL`, якщо це дозволено (потрібно додати `null=True` до поля).
     - `models.SET_DEFAULT`: Встановити поле у значення за замовчуванням.
     - `models.SET()`: Встановити значення, визначене переданою функцією.
     - `models.DO_NOTHING`: Нічого не робити, ви повинні самостійно обробляти цю ситуацію.
   - Приклад: `author = models.ForeignKey(Author, on_delete=models.CASCADE)`

3. **`related_name`**:
   - Опціональний аргумент, який задає ім'я зворотного зв'язку для доступу до пов'язаних об'єктів з іншої моделі.
   - Приклад: `author = models.ForeignKey(Author, on_delete=models.CASCADE, related_name='books')`
   - Тепер ви можете отримати всі книги автора так: `author.books.all()`

4. **`related_query_name`**:
   - Опціональний аргумент, який задає ім'я для використання у запитах.
   - Приклад: `author = models.ForeignKey(Author, on_delete=models.CASCADE, related_query_name='book')`
   - Тепер ви можете виконувати запити на книги через автора так: `Author.objects.filter(book__title__icontains='Python')`

5. **`to_field`**:
   - Опціональний аргумент, який вказує на конкретне поле в пов'язаній моделі, до якого створюється зв'язок (за замовчуванням це первинний ключ).
   - Приклад: `author = models.ForeignKey(Author, on_delete=models.CASCADE, to_field='name')`

6. **`db_constraint`**:
   - Опціональний аргумент, який визначає, чи слід створювати обмеження бази даних для цього зв'язку. За замовчуванням `True`.
   - Приклад: `author = models.ForeignKey(Author, on_delete=models.CASCADE, db_constraint=False)`

7. **`swappable`**:
   - Опціональний аргумент, який визначає, чи дозволяється обмін моделями, коли встановлено як `False`, це запобігає обміну.

### Приклади використання

#### Зв'язок багато-до-одного з каскадним видаленням
```python
class Author(models.Model):
    name = models.CharField(max_length=100)

class Book(models.Model):
    title = models.CharField(max_length=200)
    author = models.ForeignKey(Author, on_delete=models.CASCADE)
```
У цьому прикладі, коли автор буде видалений, всі книги цього автора будуть також видалені завдяки `on_delete=models.CASCADE`.

#### Зворотний зв'язок та запити
```python
class Author(models.Model):
    name = models.CharField(max_length=100)

class Book(models.Model):
    title = models.CharField(max_length=200)
    author = models.ForeignKey(Author, on_delete=models.CASCADE, related_name='books')

# Використання у вигляді:
author = Author.objects.get(id=1)
books = author.books.all()  # Отримати всі книги автора
```

#### Захист від видалення
```python
class Author(models.Model):
    name = models.CharField(max_length=100)

class Book(models.Model):
    title = models.CharField(max_length=200)
    author = models.ForeignKey(Author, on_delete=models.PROTECT)
```
У цьому прикладі, якщо ви спробуєте видалити автора, який має книги, буде піднята помилка `ProtectedError`.

### Висновок

Поле `ForeignKey` в моделях Django є потужним інструментом для встановлення зв'язків "багато-до-одного" між моделями. Воно підтримує різні атрибути, які дозволяють налаштувати поведінку при видаленні пов'язаних об'єктів, а також налаштувати зворотні зв'язки і запити для більш зручного доступу до даних.

## ManyToManyField
Поле `ManyToManyField` в моделях Django використовується для створення зв'язків "багато-до-багатьох" між моделями. Це означає, що один екземпляр однієї моделі може бути пов'язаний з багатьма екземплярами іншої моделі, і навпаки.
[Model field reference | Django documentation | Django (djangoproject.com)](https://docs.djangoproject.com/en/5.0/ref/models/fields/#manytomanyfield)

### Основні характеристики та атрибути `ManyToManyField`

1. **`to`**:
   - Обов'язковий аргумент, який вказує на модель, до якої створюється зв'язок. Це може бути або клас моделі, або назва моделі у вигляді рядка.
   - Приклад: `tags = models.ManyToManyField(Tag)`

2. **`related_name`**:
   - Опціональний аргумент, який задає ім'я зворотного зв'язку для доступу до пов'язаних об'єктів з іншої моделі.
   - Приклад: `tags = models.ManyToManyField(Tag, related_name='articles')`
   - Тепер ви можете отримати всі статті, пов'язані з тегом: `tag.articles.all()`

3. **`related_query_name`**:
   - Опціональний аргумент, який задає ім'я для використання у запитах.
   - Приклад: `tags = models.ManyToManyField(Tag, related_query_name='article')`
   - Тепер ви можете виконувати запити на статті через тег: `Tag.objects.filter(article__title__icontains='Python')`

4. **`through`**:
   - Опціональний аргумент, який вказує на проміжну модель для користувацького зв'язку.
   - Приклад: `tags = models.ManyToManyField(Tag, through='ArticleTag')`
   - Дивіться приклад з проміжною моделлю нижче.

5. **`through_fields`**:
   - Використовується разом з `through` для вказання полів, які використовуються для створення зв'язку.
   - Приклад: `tags = models.ManyToManyField(Tag, through='ArticleTag', through_fields=('article', 'tag'))`

6. **`db_table`**:
   - Опціональний аргумент, який задає назву таблиці в базі даних для зв'язку багато-до-багатьох.
   - Приклад: `tags = models.ManyToManyField(Tag, db_table='article_tag_table')`

7. **`symmetrical`**:
   - Використовується для зв'язків в моделі з самою собою (за замовчуванням `True` для самих себе, і `False` для інших моделей).
   - Приклад: `friends = models.ManyToManyField('self', symmetrical=False)`

### Приклади використання

#### Простий зв'язок багато-до-багатьох

```python
from django.db import models

class Tag(models.Model):
    name = models.CharField(max_length=100)

class Article(models.Model):
    title = models.CharField(max_length=200)
    content = models.TextField()
    tags = models.ManyToManyField(Tag)
```

У цьому прикладі кожна стаття може мати багато тегів, і кожен тег може бути пов'язаний з багатьма статтями.

#### Зворотний зв'язок та запити

```python
# Використання у вигляді:
article = Article.objects.get(id=1)
tags = article.tags.all()  # Отримати всі теги для статті

tag = Tag.objects.get(id=1)
articles = tag.article_set.all()  # Отримати всі статті для тега
```

#### З користувацькою проміжною моделлю

```python
class Tag(models.Model):
    name = models.CharField(max_length=100)

class Article(models.Model):
    title = models.CharField(max_length=200)
    content = models.TextField()
    tags = models.ManyToManyField(Tag, through='ArticleTag')

class ArticleTag(models.Model):
    article = models.ForeignKey(Article, on_delete=models.CASCADE)
    tag = models.ForeignKey(Tag, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
```

У цьому прикладі проміжна модель `ArticleTag` використовується для зберігання додаткових даних про зв'язок між статтями та тегами, таких як дата створення зв'язку.

### Створення та використання зв'язків багато-до-багатьох

#### Додавання зв'язків

```python
# Створення об'єктів
article = Article.objects.create(title='My Article', content='Content of the article')
tag1 = Tag.objects.create(name='Python')
tag2 = Tag.objects.create(name='Django')

# Додавання тегів до статті
article.tags.add(tag1, tag2)

# Видалення тега зі статті
article.tags.remove(tag1)

# Призначення тегів (видаляє попередні)
article.tags.set([tag1, tag2])

# Очистити всі теги зі статті
article.tags.clear()
```

### Підсумок

Поле `ManyToManyField` в моделях Django є потужним інструментом для створення зв'язків "багато-до-багатьох" між моделями. Воно підтримує різні атрибути для налаштування поведінки та дозволяє зручно працювати зі зв'язками через шаблонну мову Django та ORM.

## OneToOneField
Поле `OneToOneField` в моделях Django використовується для створення зв'язків "один-до-одного" між моделями. Це означає, що кожен екземпляр однієї моделі пов'язаний з одним і тільки одним екземпляром іншої моделі. Такий тип зв'язку часто використовується для розширення користувацьких моделей або створення додаткових інформаційних полів для існуючих моделей.
[Model field reference | Django documentation | Django (djangoproject.com)](https://docs.djangoproject.com/en/5.0/ref/models/fields/#onetoonefield)

### Основні характеристики та атрибути `OneToOneField`

1. **`to`**:
   - Обов'язковий аргумент, який вказує на модель, до якої створюється зв'язок. Це може бути або клас моделі, або назва моделі у вигляді рядка.
   - Приклад: `profile = models.OneToOneField(Profile, on_delete=models.CASCADE)`

2. **`on_delete`**:
   - Обов'язковий аргумент, який визначає поведінку при видаленні пов'язаної моделі. Значення можуть бути такі ж, як і для `ForeignKey`:
     - `models.CASCADE`: Видалити об'єкт, на який посилається це поле.
     - `models.PROTECT`: Запобігти видаленню об'єкта, на який посилається це поле, і підняти помилку `ProtectedError`.
     - `models.SET_NULL`: Встановити поле у значення `NULL`, якщо це дозволено (потрібно додати `null=True` до поля).
     - `models.SET_DEFAULT`: Встановити поле у значення за замовчуванням.
     - `models.SET()`: Встановити значення, визначене переданою функцією.
     - `models.DO_NOTHING`: Нічого не робити, ви повинні самостійно обробляти цю ситуацію.
   - Приклад: `profile = models.OneToOneField(Profile, on_delete=models.CASCADE)`

3. **`primary_key`**:
   - Опціональний аргумент, який визначає, чи є це поле первинним ключем моделі. За замовчуванням це `False`.
   - Приклад: `profile = models.OneToOneField(Profile, on_delete=models.CASCADE, primary_key=True)`

4. **`related_name`**:
   - Опціональний аргумент, який задає ім'я зворотного зв'язку для доступу до пов'язаних об'єктів з іншої моделі.
   - Приклад: `profile = models.OneToOneField(Profile, on_delete=models.CASCADE, related_name='user_profile')`
   - Тепер ви можете отримати профіль користувача так: `user.user_profile`

5. **`related_query_name`**:
   - Опціональний аргумент, який задає ім'я для використання у запитах.
   - Приклад: `profile = models.OneToOneField(Profile, on_delete=models.CASCADE, related_query_name='user')`
   - Тепер ви можете виконувати запити на користувачів через профіль так: `Profile.objects.filter(user__username='john')`

6. **`to_field`**:
   - Опціональний аргумент, який вказує на конкретне поле в пов'язаній моделі, до якого створюється зв'язок (за замовчуванням це первинний ключ).
   - Приклад: `profile = models.OneToOneField(Profile, on_delete=models.CASCADE, to_field='profile_id')`

### Приклад використання

#### Створення профілю для користувача

Припустимо, що у вас є модель `User`, і ви хочете створити додаткову інформацію для кожного користувача у вигляді моделі `Profile`.

```python
from django.db import models
from django.contrib.auth.models import User

class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    bio = models.TextField()
    birth_date = models.DateField(null=True, blank=True)
    location = models.CharField(max_length=100, null=True, blank=True)

    def __str__(self):
        return self.user.username
```

### Створення та доступ до зв'язків один-до-одного

#### Створення профілю

```python
user = User.objects.create(username='john', password='password')
profile = Profile.objects.create(user=user, bio='Hello, I am John!', birth_date='1990-01-01', location='New York')
```

#### Доступ до профілю через користувача

```python
user = User.objects.get(username='john')
profile = user.profile
print(profile.bio)  # Виведе: Hello, I am John!
```

#### Доступ до користувача через профіль

```python
profile = Profile.objects.get(user__username='john')
user = profile.user
print(user.username)  # Виведе: john
```

### Підсумок

Поле `OneToOneField` в моделях Django є потужним інструментом для створення зв'язків "один-до-одного" між моделями. Воно підтримує різні атрибути для налаштування поведінки та дозволяє зручно працювати зі зв'язками через шаблонну мову Django та ORM. Це поле часто використовується для розширення існуючих моделей додатковою інформацією або функціональністю.
## DateField

Поле `DateField` в моделях Django використовується для зберігання дати (без часу) в базі даних. Воно надає можливість зберігати, валідувати та обробляти дати в різних форматах. 
[Model field reference | Django documentation | Django (djangoproject.com)](https://docs.djangoproject.com/en/5.0/ref/models/fields/#datefield)

### Основні атрибути

1. **`auto_now`**:
   - Якщо встановлено як `True`, поле автоматично оновлюється до поточної дати при кожному збереженні об'єкта. Не можна використовувати одночасно з `auto_now_add`.
   - Приклад: `created_at = models.DateField(auto_now=True)`

2. **`auto_now_add`**:
   - Якщо встановлено як `True`, поле автоматично встановлюється на поточну дату при першому створенні об'єкта. Не можна використовувати одночасно з `auto_now`.
   - Приклад: `created_at = models.DateField(auto_now_add=True)`

3. **`null`**:
   - Якщо встановлено як `True`, поле може зберігати значення `NULL` в базі даних.
   - Приклад: `birth_date = models.DateField(null=True)`

4. **`blank`**:
   - Якщо встановлено як `True`, поле може бути порожнім у формах.
   - Приклад: `birth_date = models.DateField(blank=True)`

5. **`default`**:
   - Встановлює значення за замовчуванням для поля.
   - Приклад: `start_date = models.DateField(default=datetime.date.today)`

6. **`choices`**:
   - Задає набір виборів для поля.
   - Приклад:
     ```python
     YEAR_IN_SCHOOL_CHOICES = [
         ('FR', 'Freshman'),
         ('SO', 'Sophomore'),
         ('JR', 'Junior'),
         ('SR', 'Senior'),
     ]
     year_in_school = models.CharField(max_length=2, choices=YEAR_IN_SCHOOL_CHOICES)
     ```

7. **`verbose_name`**:
   - Людинозрозуміле ім'я поля.
   - Приклад: `birth_date = models.DateField(verbose_name='Date of Birth')`

8. **`help_text`**:
   - Пояснювальний текст для поля, який відображається в адміністративному інтерфейсі.
   - Приклад: `birth_date = models.DateField(help_text='Please use the following format: <em>YYYY-MM-DD</em>.')`

### Приклади використання

#### Проста модель з полем `DateField`

```python
from django.db import models

class Event(models.Model):
    name = models.CharField(max_length=200)
    event_date = models.DateField()

    def __str__(self):
        return f"{self.name} on {self.event_date}"
```

#### Модель з `auto_now` та `auto_now_add`

```python
from django.db import models

class Article(models.Model):
    title = models.CharField(max_length=200)
    content = models.TextField()
    created_at = models.DateField(auto_now_add=True)  # Автоматично встановлює дату при створенні
    updated_at = models.DateField(auto_now=True)     # Автоматично оновлює дату при кожному збереженні

    def __str__(self):
        return self.title
```

#### Використання з формами

При використанні поля `DateField` у формах Django, воно може автоматично створювати відповідні HTML елементи для введення дати.

```python
from django import forms
from .models import Event

class EventForm(forms.ModelForm):
    class Meta:
        model = Event
        fields = ['name', 'event_date']
```

### Валідація

Django автоматично здійснює валідацію поля `DateField`, перевіряючи, чи введене значення є коректною датою.

### Підсумок

Поле `DateField` в моделях Django дозволяє зберігати дати без часу, з підтримкою автоматичного встановлення дати при створенні або оновленні об'єкта. Воно підтримує широкий спектр атрибутів для налаштування поведінки та валідації поля, що робить його потужним інструментом для роботи з датами в додатках Django.