Метадані моделі в Django — це додаткова інформація, що налаштовує поведінку моделі. Вони визначаються в класі `Meta`, який є внутрішнім класом у вашій моделі. За допомогою метаданих ви можете налаштувати різні аспекти моделі, такі як ім'я таблиці в базі даних, сортування за замовчуванням, унікальні обмеження тощо.

### Основні параметри класу `Meta`

#### 1. `db_table`
Визначає ім'я таблиці в базі даних. Якщо не задано, Django автоматично генерує ім'я таблиці, використовуючи ім'я застосунку та ім'я моделі.

```python
class Book(models.Model):
    title = models.CharField(max_length=200)

    class Meta:
        db_table = 'myapp_book'
```

#### 2. `ordering`
Визначає порядок сортування об'єктів за замовчуванням. Це може бути список полів, за якими повинно відбуватися сортування.

```python
class Book(models.Model):
    title = models.CharField(max_length=200)
    published_date = models.DateField()

    class Meta:
        ordering = ['published_date']
```

#### 3. `verbose_name` і `verbose_name_plural`
Визначають читабельні назви для моделі в однині та множині. Вони використовуються в адміністративній частині Django.

```python
class Book(models.Model):
    title = models.CharField(max_length=200)

    class Meta:
        verbose_name = 'Книга'
        verbose_name_plural = 'Книги'
```

#### 4. `unique_together`
Забезпечує унікальність комбінації значень кількох полів.

```python
class Book(models.Model):
    title = models.CharField(max_length=200)
    author = models.ForeignKey(Author, on_delete=models.CASCADE)

    class Meta:
        unique_together = ['title', 'author']
```

#### 5. `index_together`
Забезпечує індексацію кількох полів разом для підвищення продуктивності запитів, які фільтрують за цими полями.

```python
class Book(models.Model):
    title = models.CharField(max_length=200)
    author = models.ForeignKey(Author, on_delete=models.CASCADE)

    class Meta:
        index_together = ['title', 'author']
```

#### 6. `constraints`
Визначає додаткові обмеження на рівні бази даних.

```python
from django.db import models
from django.db.models import Q

class Book(models.Model):
    title = models.CharField(max_length=200)
    author = models.ForeignKey(Author, on_delete=models.CASCADE)
    published_date = models.DateField()

    class Meta:
        constraints = [
            models.CheckConstraint(
                check=Q(published_date__lte='2024-05-17'),
                name='published_date_check'
            )
        ]
```

#### 7. `permissions`
Додає спеціальні дозволи для моделі.

```python
class Book(models.Model):
    title = models.CharField(max_length=200)

    class Meta:
        permissions = [
            ('can_publish', 'Can publish book'),
        ]
```

### Додаткові параметри класу `Meta`

#### 8. `abstract`
Якщо встановлено в `True`, модель буде абстрактною і не створить таблиці в базі даних. Це корисно для створення базових класів, від яких успадковуються інші моделі.

```python
class CommonInfo(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True

class Student(CommonInfo):
    name = models.CharField(max_length=100)
```

#### 9. `default_related_name`
Визначає ім'я зворотного зв'язку за замовчуванням для `ForeignKey`, `OneToOneField`, і `ManyToManyField`.

```python
class Author(models.Model):
    name = models.CharField(max_length=100)

    class Meta:
        default_related_name = 'books'

class Book(models.Model):
    title = models.CharField(max_length=200)
    author = models.ForeignKey(Author, on_delete=models.CASCADE)
```

Тепер ви можете використовувати `author.books.all()` для отримання всіх книг автора.

#### 10. `get_latest_by`
Визначає поле, яке використовується для методів `latest()` і `earliest()`.

```python
class Book(models.Model):
    title = models.CharField(max_length=200)
    published_date = models.DateField()

    class Meta:
        get_latest_by = 'published_date'
```

### Приклад повної моделі з метаданими

```python
class Book(models.Model):
    title = models.CharField(max_length=200)
    author = models.ForeignKey(Author, on_delete=models.CASCADE)
    published_date = models.DateField()
    isbn = models.CharField(max_length=13)

    class Meta:
        db_table = 'library_books'
        ordering = ['-published_date']
        verbose_name = 'Book'
        verbose_name_plural = 'Books'
        unique_together = ['title', 'author']
        index_together = ['author', 'published_date']
        permissions = [('can_publish', 'Can publish book')]
        constraints = [
            models.CheckConstraint(
                check=Q(published_date__lte='2024-05-17'),
                name='published_date_check'
            )
        ]
```

### Висновок

Метадані моделі в Django дозволяють налаштовувати поведінку та структуру моделі більш гнучко та детально. Вони забезпечують можливість додавання обмежень, налаштування імен таблиць, сортування даних, визначення спеціальних дозволів та багато іншого. Використання метаданих допомагає створювати більш оптимізовані та керовані моделі, що сприяє ефективній розробці веб-додатків.