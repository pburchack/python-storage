
Моделі в Django є центральною частиною системи ORM (Object-Relational Mapping), яка дозволяє розробникам працювати з базами даних за допомогою об'єктно-орієнтованого підходу. Кожна модель у Django відповідає таблиці в базі даних, а кожен екземпляр моделі — рядку в цій таблиці. Моделі визначають структуру таблиці (поля) та бізнес-логіку для роботи з даними.

### Основні поняття

1. **Клас моделі**: Визначається як підклас `django.db.models.Model` і представляє таблицю в базі даних.
2. **Поля моделі**: Атрибути класу моделі, які визначають колонки таблиці в базі даних. Django надає багато типів полів, таких як `CharField`, `IntegerField`, `DateTimeField` тощо.
3. **Метадані (Meta)**: Клас внутрішньої мета-інформації, який дозволяє налаштовувати поведінку моделі, наприклад, задавати ім'я таблиці, сортування за замовчуванням тощо.
4. **Методи моделі**: Функції, визначені в класі моделі, які забезпечують додаткову бізнес-логіку та функціональність.

### Визначення моделі

Модель визначається як клас, який успадковується від `django.db.models.Model`. Ось приклад простої моделі:

```python
from django.db import models

class Author(models.Model):
    name = models.CharField(max_length=100)
    birth_date = models.DateField()

    def __str__(self):
        return self.name

class Book(models.Model):
    title = models.CharField(max_length=200)
    author = models.ForeignKey(Author, on_delete=models.CASCADE)
    published_date = models.DateField()
    isbn = models.CharField(max_length=13)

    def __str__(self):
        return self.title
```

### [[Основні типи полів]]

- **CharField**: Використовується для зберігання коротких рядків тексту. Необхідно вказати максимальну довжину.
- **TextField**: Використовується для зберігання довгих текстових даних.
- **IntegerField**: Зберігає цілі числа.
- **FloatField**: Зберігає числа з плаваючою комою.
- **DateField і DateTimeField**: Зберігають дату та дату з часом відповідно.
- **BooleanField**: Зберігає значення True або False.
- **ForeignKey**: Встановлює зв'язок "багато до одного" з іншою моделлю.
- **ManyToManyField**: Встановлює зв'язок "багато до багатьох" з іншою моделлю.
- **OneToOneField**: Встановлює зв'язок "один до одного" з іншою моделлю.

### [[Метадані (Meta)]]

Клас `Meta` дозволяє задавати додаткові параметри для моделі. Наприклад:

```python
class Book(models.Model):
    title = models.CharField(max_length=200)
    author = models.ForeignKey(Author, on_delete=models.CASCADE)
    published_date = models.DateField()
    isbn = models.CharField(max_length=13)

    class Meta:
        ordering = ['published_date']
        verbose_name = 'book'
        verbose_name_plural = 'books'

    def __str__(self):
        return self.title
```

### [[Методи моделі]] 

Методи моделі визначають бізнес-логіку, пов'язану з даними. Django автоматично надає деякі методи, такі як `save()`, `delete()`, але ви можете визначати власні методи для специфічної логіки:

```python
class Author(models.Model):
    name = models.CharField(max_length=100)
    birth_date = models.DateField()

    def __str__(self):
        return self.name

    def get_books(self):
        return self.book_set.all()
```

### Робота з моделями

Після визначення моделей ви можете використовувати їх для взаємодії з базою даних через ORM:

- **Створення об'єкта**:

```python
author = Author(name='George Orwell', birth_date='1903-06-25')
author.save()
```

- **Запит до бази даних**:

```python
authors = Author.objects.all()
orwell = Author.objects.get(name='George Orwell')
books_by_orwell = Book.objects.filter(author=orwell)
```

- **Оновлення об'єкта**:

```python
orwell.birth_date = '1903-06-26'
orwell.save()
```

- **Видалення об'єкта**:

```python
orwell.delete()
```

### Висновок

Моделі в Django є потужним інструментом для визначення структури бази даних та бізнес-логіки вашого додатка. Вони дозволяють легко взаємодіяти з даними за допомогою об'єктно-орієнтованого підходу, спрощуючи розробку та підтримку веб-додатків.
