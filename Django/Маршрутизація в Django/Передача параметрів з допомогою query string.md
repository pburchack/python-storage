#url #query_string
Передача параметрів за допомогою query string у Django дозволяє передавати дані у вигляді ключ-значення через URL. Цей метод є дуже зручним для передачі додаткових даних з одного веб-ресурсу до іншого без зміни URL-патернів. Параметри query string передаються після символу `?` в URL і розділяються символом `&`.

### Приклад URL з query string

```url
http://example.com/search/?q=django&sort=recent
```

У цьому прикладі:
- `q` - параметр запиту зі значенням `django`.
- `sort` - параметр запиту зі значенням `recent`.

### Отримання параметрів query string у представленнях Django

Django надає зручний спосіб доступу до параметрів query string через об'єкт `request`.

#### Кроки для отримання параметрів query string

1. **Визначте URL**: У конфігурації URL ви визначаєте маршрут без врахування параметрів query string.
2. **Отримайте параметри query string у представленні**: Використовуйте `request.GET` для доступу до параметрів.

#### Приклад

**Крок 1: Визначення URL**

У файлі `urls.py` визначте маршрут для представлення:

```python
from django.urls import path
from . import views

urlpatterns = [
    path('search/', views.search, name='search'),
]
```

**Крок 2: Обробка параметрів query string у представленні**

У файлі `views.py` визначте представлення, яке буде обробляти параметри query string:

```python
from django.http import HttpResponse

def search(request):
    query = request.GET.get('q', '')  # Отримуємо значення параметра 'q', або порожній рядок за замовчуванням
    sort = request.GET.get('sort', 'default')  # Отримуємо значення параметра 'sort', або 'default' за замовчуванням
    response_text = f"Search query: {query}, Sort order: {sort}"
    return HttpResponse(response_text)
```

### Переваги використання query string

1. **Гнучкість**: Можливість динамічно передавати додаткові параметри без зміни структури URL.
2. **Простота**: Легкий спосіб передавати дані між сторінками або компонентами додатку.
3. **Сумісність**: Query string добре підтримуються у всіх веб-браузерах та бібліотеках.

### Приклади використання query string у Django

#### Фільтрація та сортування

Часто використовується для фільтрації або сортування даних у списках.

```python
def product_list(request):
    category = request.GET.get('category')
    sort_by = request.GET.get('sort', 'name')
    products = Product.objects.all()

    if category:
        products = products.filter(category=category)
    
    if sort_by == 'price':
        products = products.order_by('price')
    else:
        products = products.order_by('name')

    return render(request, 'product_list.html', {'products': products})
```

#### Пагінація

Зазвичай використовується для передавання інформації про поточну сторінку при пагінації.

```python
from django.core.paginator import Paginator

def product_list(request):
    product_list = Product.objects.all()
    paginator = Paginator(product_list, 10)  # Показувати 10 продуктів на сторінку

    page_number = request.GET.get('page')
    page_obj = paginator.get_page(page_number)

    return render(request, 'product_list.html', {'page_obj': page_obj})
```

### Практичні поради

1. **Перевірка наявності параметрів**:
   Завжди перевіряйте наявність параметрів у `request.GET` за допомогою методу `get`, щоб уникнути помилок при відсутності параметра.

   ```python
   param = request.GET.get('param', 'default_value')
   ```

2. **Безпека**:
   Переконайтеся, що параметри query string, які ви отримуєте, безпечні для використання у вашій програмі. Наприклад, при роботі з числовими значеннями, можна використовувати функцію `int()` для конвертації значень і перевірки їхньої коректності.

   ```python
   try:
       page = int(request.GET.get('page', 1))
   except ValueError:
       page = 1
   ```

3. **Кодування URL**:
   Якщо ви передаєте значення, що можуть містити спеціальні символи, переконайтеся, що вони закодовані належним чином. Django надає функції для кодування та декодування URL-параметрів.

### Висновок

Передача параметрів за допомогою query string є зручним і гнучким способом передавати додаткові дані через URL у Django. Використовуючи `request.GET`, можна легко отримувати ці параметри і використовувати їх у представленнях для реалізації різних функцій, таких як фільтрація, сортування, пагінація тощо.