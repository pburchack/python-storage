#url #шаблон #маршрут 
У Django URL-обробка здійснюється за допомогою визначення шаблонів маршрутів у файлі `urls.py` за допомогою функції `path()`. Ця функція дозволяє визначати, які URL повинні оброблятися якими представленнями (views). Важливо дотримуватися певних правил при складанні шаблонів атрибута `route` для забезпечення правильного розпізнавання та обробки URL.

### Правила складання шаблонів `route`:

1. **Статичні шляхи**: Простий шаблон без динамічних сегментів.
2. **Динамічні сегменти**: Використання змінних для захоплення частин URL.
3. **Конвертери типів**: Вказівка типу даних для динамічних сегментів.
4. **Ієрархічні маршрути**: Вкладені шляхи для організації маршрутів.
5. **Опціональні сегменти**: Обробка опціональних частин URL.

### Синтаксис:

```python
path(route, view, kwargs=None, name=None)
```

- **route**: Шаблон URL.
- **view**: Представлення, яке обробляє запит.
- **kwargs**: Додаткові аргументи для представлення.
- **name**: Ім'я маршруту для зручності зворотного виклику.

### Приклади:

#### 1. **Статичні шляхи**

```python
from django.urls import path
from . import views

urlpatterns = [
    path('about/', views.about, name='about'),
]
```

URL `/about/` буде оброблятися представленням `views.about`.

#### 2. **Динамічні сегменти**

```python
urlpatterns = [
    path('post/<int:post_id>/', views.post_detail, name='post_detail'),
]
```

URL типу `/post/5/` передає значення `5` як `post_id` у представлення `views.post_detail`.

#### 3. **Конвертери типів**

- `str`: Рядок (за замовчуванням).
- `int`: Ціле число.
- `slug`: Слаг (літери, цифри, тире, підкреслення).
- `uuid`: UUID.
- `path`: Текстовий шлях, що включає `/`.

```python
urlpatterns = [
    path('user/<str:username>/', views.user_profile, name='user_profile'),
    path('post/<int:post_id>/', views.post_detail, name='post_detail'),
    path('article/<slug:slug>/', views.article_detail, name='article_detail'),
    path('document/<uuid:doc_id>/', views.document_view, name='document_view'),
    path('files/<path:filepath>/', views.serve_file, name='serve_file'),
]
```

#### 4. **Ієрархічні маршрути**

```python
urlpatterns = [
    path('blog/', views.blog_index, name='blog_index'),
    path('blog/<int:year>/', views.blog_year_archive, name='blog_year_archive'),
    path('blog/<int:year>/<int:month>/', views.blog_month_archive, name='blog_month_archive'),
    path('blog/<int:year>/<int:month>/<int:day>/', views.blog_day_archive, name='blog_day_archive'),
    path('blog/<int:year>/<int:month>/<int:day>/<slug:slug>/', views.blog_detail, name='blog_detail'),
]
```

Ці маршрути дозволяють створювати ієрархічну структуру для блогу.

#### 5. **Опціональні сегменти**

Опціональні сегменти не підтримуються напряму у функції `path()`, але можна використовувати додаткову обробку в представленні для роботи з опціональними частинами URL.

```python
from django.urls import path, re_path
from . import views

urlpatterns = [
    path('category/<int:category_id>/', views.category, name='category'),
    re_path(r'^category/(?P<category_id>\d+)/(?P<optional_param>\w+)?/$', views.category, name='category_optional'),
]
```

У представленні `views.category` перевіряється наявність `optional_param`:

```python
def category(request, category_id, optional_param=None):
    if optional_param:
        # обробка з optional_param
    else:
        # обробка без optional_param
    return HttpResponse(f"Category ID: {category_id}, Optional Param: {optional_param}")
```

### Висновок

Визначення шаблонів атрибута `route` у функції `path()` є важливою частиною маршрутизації у Django. Дотримання наведених правил та використання різних типів конвертерів дозволяє створювати гнучкі та потужні маршрути для обробки різноманітних URL-запитів у вашому додатку.