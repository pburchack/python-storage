#url #конвертер
Передача параметрів за допомогою URL patterns в Django дозволяє направляти значення з URL у відповідні представлення (views). Це досягається шляхом визначення динамічних сегментів у шаблонах URL. Ці сегменти можуть бути захоплені і передані як аргументи до функцій представлень.

### Основні кроки передачі параметрів через URL patterns:


1. **Визначення динамічних сегментів у URL patterns**.
2. **Обробка захоплених параметрів у функціях представлень**.

### Використання вбудованих конвертерів

Django підтримує кілька типів конвертерів для визначення типу даних динамічних сегментів у URL. Найпоширеніші конвертери включають `str`, `int`, `slug`, `uuid`, та `path`.

#### Приклади використання конвертерів

1. **`str` конвертер** (рядковий тип):
    ```python
    from django.urls import path
    from . import views

    urlpatterns = [
        path('user/<str:username>/', views.user_profile, name='user_profile'),
    ]
    ```

    Представлення `user_profile` отримає параметр `username` як рядок.
    
    ```python
    def user_profile(request, username):
        # обробка параметра username
        return HttpResponse(f"User: {username}")
    ```

2. **`int` конвертер** (ціле число):
    ```python
    from django.urls import path
    from . import views

    urlpatterns = [
        path('post/<int:post_id>/', views.post_detail, name='post_detail'),
    ]
    ```

    Представлення `post_detail` отримає параметр `post_id` як ціле число.
    
    ```python
    def post_detail(request, post_id):
        # обробка параметра post_id
        return HttpResponse(f"Post ID: {post_id}")
    ```

3. **`slug` конвертер** (текстовий слаг):
    ```python
    from django.urls import path
    from . import views

    urlpatterns = [
        path('article/<slug:slug>/', views.article_detail, name='article_detail'),
    ]
    ```

    Представлення `article_detail` отримає параметр `slug` як текстовий слаг.
    
    ```python
    def article_detail(request, slug):
        # обробка параметра slug
        return HttpResponse(f"Article Slug: {slug}")
    ```

4. **`uuid` конвертер** (UUID):
    ```python
    from django.urls import path
    from . import views

    urlpatterns = [
        path('item/<uuid:item_id>/', views.item_detail, name='item_detail'),
    ]
    ```

    Представлення `item_detail` отримає параметр `item_id` як UUID.
    
    ```python
    def item_detail(request, item_id):
        # обробка параметра item_id
        return HttpResponse(f"Item ID: {item_id}")
    ```

5. **`path` конвертер** (шлях, що включає `/`):
    ```python
    from django.urls import path
    from . import views

    urlpatterns = [
        path('files/<path:subpath>/', views.serve_file, name='serve_file'),
    ]
    ```

    Представлення `serve_file` отримає параметр `subpath` як рядок, що включає `/`.
    
    ```python
    def serve_file(request, subpath):
        # обробка параметра subpath
        return HttpResponse(f"Serving file at: {subpath}")
    ```

### Створення власних конвертерів

Якщо вбудованих конвертерів недостатньо, можна створити власний конвертер.

#### Крок 1: Створення класу конвертера

```python
class FourDigitYearConverter:
    regex = '[0-9]{4}'

    def to_python(self, value):
        return int(value)

    def to_url(self, value):
        return '%04d' % value
```

#### Крок 2: Реєстрація конвертера

У файлі `urls.py` потрібно зареєструвати конвертер:

```python
from django.urls import register_converter, path
from . import views
from .converters import FourDigitYearConverter

register_converter(FourDigitYearConverter, 'yyyy')

urlpatterns = [
    path('year/<yyyy:year>/', views.year_archive, name='year_archive'),
]
```

#### Крок 3: Використання у представленнях

```python
def year_archive(request, year):
    # обробка параметра year
    return HttpResponse(f"Year: {year}")
```

### Переваги передачі параметрів через URL patterns

1. **Динамічність**: Можливість захоплювати динамічні значення з URL, що дозволяє створювати гнучкі та інтерактивні веб-сторінки.
2. **Читабельність**: URL з чітко визначеними параметрами легше читати та розуміти.
3. **Типобезпека**: Використання конвертерів дозволяє гарантувати правильний тип даних для параметрів, що передаються у представлення.

### Висновок

Передача параметрів через URL patterns у Django є важливою частиною маршрутизації. Використовуючи вбудовані конвертери та можливість створення власних, можна ефективно захоплювати та обробляти динамічні значення з URL, що робить веб-додатки більш інтерактивними та зручними у використанні.