Приклад типового методу для пошуку з допомогою [[queryset]]:
#### Розберемо метод `get_queryset` детально :

```Python
from typing import Any
from django.db.models import Q
from django.db.models.query import QuerySet
from .models import Products
from django.views.generic import ListView


class ProductListView(ListView):
    model = Products
    template_name = 'calculator/page_of.html'
    paginate_by = 10  # Number of products per page
    ordering = ['name']

	def get_queryset(self) -> QuerySet[Any]:
        queryset = super().get_queryset()
        search_query = self.request.GET.get('search', '').strip()
        if search_query:
            queryset = queryset.filter(
                Q(name__icontains=search_query) |
                Q(name__iregex=r'\b{}\b'.format(search_query))
            )
        return queryset
        
	def get_context_data(self, **kwargs) -> dict:
        context = super().get_context_data(**kwargs)
        search_query = self.request.GET.get('search', '').strip()
        context['search_query'] = search_query
        if search_query:
            context['search_results_count'] = self.get_queryset().count()  # Counting the results for search query
        return context
```

1. `queryset = super().get_queryset()`
   - Цей рядок викликає метод `get_queryset()` батьківського класу (ListView).
   - Він отримує базовий QuerySet, який містить всі об'єкти моделі.

2. `search_query = self.request.GET.get('search', '').strip()`
   - Отримує значення параметра 'search' з GET-запиту.
   - Якщо параметр 'search' відсутній, повертає пустий рядок ''.
   - Метод `strip()` видаляє пробіли на початку та в кінці рядка.

3. `if search_query:`
   - Перевіряє, чи не є `search_query` пустим після `strip()`.
   - Якщо `search_query` не пустий, виконується фільтрація.

4. `queryset = queryset.filter(`
   - Починає застосовувати фільтр до базового QuerySet.

5. `Q(name__icontains=search_query) |`
   - `Q` об'єкт дозволяє створювати складні запити.
   - `name__icontains` шукає підрядок у полі 'name', нечутливий до регістру.
   - Наприклад, якщо `search_query` = "ябл", знайде "Яблуко", "яблучний сік" тощо.

6. `Q(name__iregex=r'\b{}\b'.format(search_query))`
   - Використовує регулярний вираз для пошуку цілого слова.
   - `\b` означає границю слова.
   - `iregex` робить пошук нечутливим до регістру.
   - Наприклад, якщо `search_query` = "яблуко", знайде "Яблуко" але не "яблучний".

7. `|` (вертикальна риска) між двома `Q` об'єктами
   - Це оператор OR. Він об'єднує два умови пошуку.
   - Запит поверне результати, які відповідають хоча б одній з умов.

8. `return queryset`
   - Повертає відфільтрований QuerySet.
   - Якщо `search_query` був пустим, повертається початковий QuerySet без фільтрації.

Додаткові зауваження:
- Метод `icontains` дуже гнучкий, але може бути повільним на великих наборах даних.
- Регулярний вираз `\b{}\b` шукає точні збіги слів, що може бути корисно для уникнення часткових збігів.
- Використання `icontains` та `iregex` робить пошук нечутливим до регістру, тобто "Яблуко" та "яблуко" будуть оброблятися однаково.

Цей метод забезпечує досить гнучкий пошук, дозволяючи знаходити як часткові збіги (завдяки `icontains`), так і повні слова (завдяки `iregex`), ігноруючи при цьому регістр символів.

#### Розберемо метод `get_context_data` детально:

1. `def get_context_data(self, **kwargs) -> dict:`
   - Це метод, який викликається для підготовки контексту, що буде переданий у шаблон.
   - `**kwargs` дозволяє методу приймати будь-яку кількість іменованих аргументів.
   - `-> dict` вказує, що метод повертає словник (це підказка типу в Python).

2. `context = super().get_context_data(**kwargs)`
   - Викликає метод `get_context_data` батьківського класу (зазвичай це ListView).
   - Це забезпечує, що базовий контекст, який надає ListView, зберігається.
   - `**kwargs` передає всі отримані аргументи в батьківський метод.

3. `search_query = self.request.GET.get('search', '').strip()`
   - Отримує значення параметра 'search' з GET-запиту.
   - Якщо параметр 'search' відсутній, повертає пустий рядок ''.
   - `strip()` видаляє пробіли на початку та в кінці рядка.

4. `context['search_query'] = search_query`
   - Додає `search_query` до контексту.
   - Це дозволяє використовувати значення пошукового запиту в шаблоні, наприклад, для відображення в полі пошуку.

5. `if search_query:`
   - Перевіряє, чи не є `search_query` пустим після `strip()`.

6. `context['search_results_count'] = self.get_queryset().count()`
   - Якщо `search_query` не пустий, додає до контексту кількість результатів пошуку.
   - `self.get_queryset()` викликає метод `get_queryset`, який ми розглядали раніше, для отримання відфільтрованого QuerySet.
   - `count()` підраховує кількість об'єктів у цьому QuerySet.

7. `return context`
   - Повертає підготовлений контекст, який буде використаний для рендерингу шаблону.

Цей метод виконує кілька важливих функцій:
- Зберігає базовий контекст, наданий ListView.
- Додає пошуковий запит до контексту, що дозволяє відображати його в шаблоні.
- Якщо був здійснений пошук, додає кількість знайдених результатів до контексту.

Це дозволяє в шаблоні:
- Відображати пошуковий запит (наприклад, "Ви шукали: {search_query}").
- Показувати кількість знайдених результатів.
- Використовувати ці дані для покращення користувацького інтерфейсу та інформативності сторінки пошуку.

Такий підхід робить ваш код більш гнучким і дружнім до користувача, надаючи додаткову інформацію про результати пошуку прямо в шаблоні.

#### Посилання
[[queryset]]