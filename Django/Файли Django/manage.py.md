#file #cmd
Файл `manage.py` є важливою частиною будь-якого проекту Django. Він знаходиться в кореневій директорії проекту і служить основним інтерфейсом командного рядка для взаємодії з вашим проектом. За допомогою цього файлу ви можете виконувати різноманітні адміністративні завдання, такі як запуск сервера розробки, застосування міграцій бази даних, створення нових додатків тощо.

### Основні функції файлу manage.py

1. **Запуск сервера розробки:** 
   ```sh
   python manage.py runserver
   ```
   Ця команда запускає вбудований сервер розробки Django, що дозволяє вам тестувати додаток локально.

2. **Застосування міграцій:**
   ```sh
   python manage.py migrate
   ```
   Виконує застосування міграцій бази даних, що забезпечує синхронізацію схеми бази даних з вашими моделями.

3. **Створення нових міграцій:**
   ```sh
   python manage.py makemigrations
   ```
   Створює нові файли міграцій на основі змін у моделях вашого додатку.

4. **Створення нового додатку:**
   ```sh
   python manage.py startapp appname
   ```
   Створює нову директорію додатку з базовою структурою, необхідною для Django-додатка.

5. **Запуск інтерактивної оболонки Django:**
   ```sh
   python manage.py shell
   ```
   Відкриває інтерактивну оболонку Python з завантаженим контекстом вашого Django-проекту.

6. **Створення суперкористувача:**
   ```sh
   python manage.py createsuperuser
   ```
   Дозволяє створити суперкористувача для доступу до адміністративного інтерфейсу Django.

### Структура файлу manage.py

Ось типовий вміст файлу `manage.py`:

```python
#!/usr/bin/env python
import os
import sys

def main():
    """Run administrative tasks."""
    os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'myproject.settings')
    try:
        from django.core.management import execute_from_command_line
    except ImportError as exc:
        raise ImportError(
            "Couldn't import Django. Are you sure it's installed and "
            "available on your PYTHONPATH environment variable? Did you "
            "forget to activate a virtual environment?"
        ) from exc
    execute_from_command_line(sys.argv)

if __name__ == '__main__':
    main()
```

### Опис коду

- **Shebang (`#!/usr/bin/env python`)**: Вказує на інтерпретатор Python, який слід використовувати для виконання цього файлу.
- **Імпорт модулів**: Імпортує необхідні модулі `os` і `sys`.
- **Функція `main()`**:
  - **Налаштування змінної оточення**: `os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'myproject.settings')` встановлює змінну оточення, яка вказує на файл налаштувань вашого проекту Django.
  - **Імпорт `execute_from_command_line`**: Імпортує функцію з Django, яка виконує команди з командного рядка.
  - **Виконання команди**: `execute_from_command_line(sys.argv)` виконує команду, яку ви ввели після `python manage.py`.

### Висновок

Файл `manage.py` є основним інструментом для управління проектом Django через командний рядок. Він забезпечує зручний інтерфейс для виконання багатьох важливих завдань, таких як запуск сервера, міграція бази даних, створення додатків і багато іншого. Цей файл допомагає структурувати процес розробки і спрощує взаємодію з проектом.