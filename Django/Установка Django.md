###### Встановлюємо віртуальне оточення
```Sh
python -m venv venv
```
###### Активуємо [[venv]]
```Sh
.venv\scripts\activate
```
###### Встановлюємо Django
```Sh
pip install django
```
###### Створюємо файл [[requirements.txt]]
```Sh
pip freeze > requirements.txt
```
###### Для створення нового проекту
```Sh
django-admin startproject name_project
```
###### Для створення проекту в готовому репозиторії
```Sh
django-admin startproject name_project .
```
###### Для створення додатку в проекті
```Sh
python manage.py startapp name_app
```
###### Запуск сервера
```Sh
python manage.py runserver
```


