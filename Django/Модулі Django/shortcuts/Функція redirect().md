#маршрут #url 
Функція `redirect()` з модуля `django.shortcuts` використовується для перенаправлення клієнта (браузера) на інший URL. Це зручний спосіб змусити клієнтський браузер відкрити нову сторінку замість поточної. `redirect()` може приймати різні типи аргументів для створення перенаправлень, включаючи URL-рядки, імена URL-шаблонів (для яких створюється зворотній виклик) або об'єкти моделей.

### Синтаксис

```python
django.shortcuts.redirect(to, permanent=False, *args, **kwargs)
```

- **to**: Може бути рядком (URL), ім'ям URL-шаблону, або об'єктом моделі.
- **permanent**: Визначає, чи є перенаправлення постійним. За замовчуванням False, тобто тимчасове перенаправлення (HTTP 302). Якщо True — постійне перенаправлення (HTTP 301).
- **args, kwargs**: Додаткові аргументи для зворотного виклику (якщо використовується ім'я URL-шаблону).

### Використання

1. **Перенаправлення на конкретний URL**:

```python
from django.shortcuts import redirect

def my_view(request):
    # деяка логіка
    return redirect('/some/url/')
```

2. **Перенаправлення з використанням імені URL-шаблону**:

```python
from django.shortcuts import redirect
from django.urls import reverse

def my_view(request):
    # деяка логіка
    return redirect('some-view-name')
```

3. **Перенаправлення з аргументами для імені URL-шаблону**:

```python
from django.shortcuts import redirect

def my_view(request, pk):
    # деяка логіка
    return redirect('some-view-name', pk=pk)
```

4. **Перенаправлення на URL, створений функцією reverse()**:

```python
from django.shortcuts import redirect
from django.urls import reverse

def my_view(request):
    url = reverse('some-view-name')
    return redirect(url)
```

5. **Перенаправлення на об'єкт моделі**:

```python
from django.shortcuts import redirect

def my_view(request, model_id):
    obj = get_object_or_404(MyModel, id=model_id)
    return redirect(obj)
```

### Приклади

#### Перенаправлення після обробки форми

```python
from django.shortcuts import render, redirect
from .forms import MyForm

def my_view(request):
    if request.method == 'POST':
        form = MyForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('success-view')
    else:
        form = MyForm()
    return render(request, 'my_template.html', {'form': form})
```

#### Перенаправлення з об'єктом моделі

```python
from django.shortcuts import redirect, get_object_or_404
from .models import MyModel

def my_view(request, model_id):
    obj = get_object_or_404(MyModel, id=model_id)
    # деяка логіка
    return redirect(obj)
```

### Переваги використання redirect()

1. **Простота використання**: `redirect()` дуже легко використовувати для створення перенаправлень у ваших представленнях.
2. **Гнучкість**: Підтримка різних типів аргументів для перенаправлення (рядки, імена URL-шаблонів, об'єкти моделей).
3. **Зручність**: Використання зворотного виклику для імен URL-шаблонів дозволяє робити код більш зрозумілим і менш залежним від змін у URL-шаблонах.

### Висновок

Функція `redirect()` з модуля `django.shortcuts` є потужним і гнучким інструментом для перенаправлення клієнтських запитів на інші URL у Django. Використовуючи різні варіанти аргументів, ви можете легко створювати як тимчасові, так і постійні перенаправлення, що робить ваш додаток більш динамічним та інтерактивним.