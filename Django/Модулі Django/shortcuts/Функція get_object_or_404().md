#exeption 
Функція `get_object_or_404()` з модуля `django.shortcuts` використовується для отримання об'єкта з бази даних за допомогою заданого запиту. Якщо об'єкт не знайдено, функція автоматично викликає виняток `Http404`, який генерує відповідь з кодом статусу 404 (Not Found). Це зручно для обробки запитів, коли очікується, що об'єкт може не існувати, і дозволяє спростити код, уникнувши необхідності вручну перевіряти, чи об'єкт існує.

### Синтаксис

```python
django.shortcuts.get_object_or_404(klass, *args, **kwargs)
```

- **klass**: Клас моделі або менеджер моделі, з якого потрібно отримати об'єкт.
- **args**: Позиційні аргументи для методу `filter()`.
- **kwargs**: Іменовані аргументи для методу `filter()`, які визначають умови запиту.

### Використання

#### Простий приклад

Уявімо, що у нас є модель `Article`:

```python
from django.shortcuts import get_object_or_404
from .models import Article

def article_detail(request, article_id):
    article = get_object_or_404(Article, id=article_id)
    return render(request, 'article_detail.html', {'article': article})
```

У цьому прикладі, якщо об'єкт з `id=article_id` не буде знайдено, буде викликано виняток `Http404`, і користувач отримає сторінку з повідомленням про помилку 404.

#### Використання з додатковими умовами

```python
from django.shortcuts import get_object_or_404
from .models import Article

def article_detail(request, article_id):
    article = get_object_or_404(Article, id=article_id, published=True)
    return render(request, 'article_detail.html', {'article': article})
```

У цьому прикладі додається додаткова умова `published=True`, тобто функція знайде об'єкт тільки якщо він має `published=True`.

#### Використання з відносинами

Якщо у вас є модель, яка має зв'язок з іншою моделлю, ви також можете використовувати `get_object_or_404()` для отримання об'єкта з урахуванням відносин.

```python
from django.shortcuts import get_object_or_404
from .models import Author, Article

def author_article_detail(request, author_id, article_id):
    author = get_object_or_404(Author, id=author_id)
    article = get_object_or_404(Article, id=article_id, author=author)
    return render(request, 'author_article_detail.html', {'article': article})
```

У цьому прикладі, якщо об'єкт `Author` або об'єкт `Article` не буде знайдено, буде викликано виняток `Http404`.

### Переваги використання

1. **Простота і зручність**: Функція дозволяє легко отримувати об'єкти з бази даних і автоматично обробляти випадки, коли об'єкт не знайдено.
2. **Чистий код**: Використання `get_object_or_404()` робить код більш читабельним і менш завантаженим перевірками на існування об'єкта.
3. **Безпека**: Функція автоматично генерує відповідь 404, що забезпечує коректну обробку запитів до неіснуючих ресурсів.

### Приклад з Django Forms

Функція `get_object_or_404()` часто використовується в комбінації з формами для обробки запитів на редагування об'єктів.

```python
from django.shortcuts import render, get_object_or_404, redirect
from .models import Article
from .forms import ArticleForm

def edit_article(request, article_id):
    article = get_object_or_404(Article, id=article_id)
    if request.method == 'POST':
        form = ArticleForm(request.POST, instance=article)
        if form.is_valid():
            form.save()
            return redirect('article_detail', article_id=article.id)
    else:
        form = ArticleForm(instance=article)
    return render(request, 'edit_article.html', {'form': form})
```

У цьому прикладі, якщо об'єкт `Article` не знайдено, функція викличе `Http404`. Якщо форма валідна, об'єкт буде збережено і користувач буде перенаправлений на сторінку деталей статті.

### Висновок

Функція `get_object_or_404()` з модуля `django.shortcuts` є потужним інструментом для зручного отримання об'єктів з бази даних і обробки ситуацій, коли об'єкт не знайдено. Вона забезпечує чистий, читабельний і безпечний код, роблячи розробку у Django більш ефективною.