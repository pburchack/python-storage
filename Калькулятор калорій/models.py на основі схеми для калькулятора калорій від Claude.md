Звісно, ось файл `models.py` на основі оновленої схеми бази даних:

```python
from django.db import models

class Product(models.Model):
    name = models.CharField(max_length=100)
    calories = models.FloatField()
    proteins = models.FloatField()
    fats = models.FloatField()
    carbohydrates = models.FloatField()
    fiber = models.FloatField()
    image = models.ImageField(upload_to='products/', null=True, blank=True)

    def __str__(self):
        return self.name

class Dish(models.Model):
    name = models.CharField(max_length=100)
    instructions = models.TextField()

    def __str__(self):
        return self.name

class DishProduct(models.Model):
    dish = models.ForeignKey(Dish, on_delete=models.CASCADE)
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    quantity = models.FloatField()

    def __str__(self):
        return f"{self.dish.name} - {self.product.name}"

class Meal(models.Model):
    MEAL_TYPES = [
        ('B', 'Breakfast'),
        ('L', 'Lunch'),
        ('D', 'Dinner'),
        ('S', 'Snack'),
    ]
    date = models.DateField()
    meal_type = models.CharField(max_length=1, choices=MEAL_TYPES)

    def __str__(self):
        return f"{self.get_meal_type_display()} on {self.date}"

class MealItem(models.Model):
    meal = models.ForeignKey(Meal, on_delete=models.CASCADE)
    product = models.ForeignKey(Product, on_delete=models.SET_NULL, null=True, blank=True)
    dish = models.ForeignKey(Dish, on_delete=models.SET_NULL, null=True, blank=True)
    quantity = models.FloatField()

    def __str__(self):
        item = self.product.name if self.product else self.dish.name
        return f"{item} in {self.meal}"

class FitnessActivity(models.Model):
    date = models.DateField()
    activity_type = models.CharField(max_length=100)
    duration = models.DurationField()
    calories_burned = models.FloatField()

    def __str__(self):
        return f"{self.activity_type} on {self.date}"

class UserGoal(models.Model):
    GOAL_TYPES = [
        ('W', 'Weight'),
        ('C', 'Calories'),
        ('A', 'Activity'),
    ]
    goal_type = models.CharField(max_length=1, choices=GOAL_TYPES)
    target_value = models.FloatField()
    start_date = models.DateField()
    end_date = models.DateField()

    def __str__(self):
        return f"{self.get_goal_type_display()} goal: {self.target_value}"

class UserProfile(models.Model):
    GENDER_CHOICES = [
        ('M', 'Male'),
        ('F', 'Female'),
        ('O', 'Other'),
    ]
    GOAL_CHOICES = [
        ('L', 'Lose weight'),
        ('M', 'Maintain weight'),
        ('G', 'Gain weight'),
    ]
    age = models.IntegerField()
    gender = models.CharField(max_length=1, choices=GENDER_CHOICES)
    height = models.FloatField()  # в сантиметрах
    weight = models.FloatField()  # в кілограмах
    activity_level = models.FloatField()  # коефіцієнт активності
    goal = models.CharField(max_length=1, choices=GOAL_CHOICES)

    def __str__(self):
        return f"User Profile (Age: {self.age}, Gender: {self.get_gender_display()})"
```

Цей файл `models.py` містить всі моделі, які ми визначили в оновленій схемі бази даних. Кожна модель представляє відповідну таблицю в базі даних.

Особливості:
- Використано відповідні типи полів Django для кожного атрибуту.
- Додано поле `image` в модель `Product` з можливістю завантаження зображень.
- Використано `ForeignKey` для створення зв'язків між моделями.
- Додано вибори (`choices`) для полів, де це доречно (наприклад, типи прийомів їжі, типи цілей, стать).
- Включено метод `__str__` для кожної моделі, щоб забезпечити зручне представлення об'єктів в адмін-панелі Django та при дебагінгу.

Щоб використовувати ці моделі, вам потрібно буде включити їх у ваш Django проект, додати відповідний додаток до `INSTALLED_APPS` у файлі `settings.py`, а потім виконати міграції за допомогою команд `python manage.py makemigrations` та `python manage.py migrate`.