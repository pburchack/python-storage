```Python
from django.db import models

class Product(models.Model):
    name = models.CharField(max_length=255)
    calories = models.FloatField()
    proteins = models.FloatField()
    fats = models.FloatField()
    carbohydrates = models.FloatField()
    fiber = models.FloatField()
    image = models.ImageField(upload_to='product_images/', null=True, blank=True)

    def __str__(self):
        return self.name

class Dish(models.Model):
    name = models.CharField(max_length=255)
    instructions = models.TextField()

    def __str__(self):
        return self.name

class DishProduct(models.Model):
    dish = models.ForeignKey(Dish, on_delete=models.CASCADE)
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    quantity = models.FloatField()

    def __str__(self):
        return f"{self.dish.name} - {self.product.name}"

class Meal(models.Model):
    MEAL_TYPES = [
        ('breakfast', 'Сніданок'),
        ('lunch', 'Обід'),
        ('dinner', 'Вечеря'),
        ('snack', 'Перекус'),
    ]

    date = models.DateField()
    meal_type = models.CharField(max_length=10, choices=MEAL_TYPES)

    def __str__(self):
        return f"{self.date} - {self.meal_type}"

class MealItem(models.Model):
    meal = models.ForeignKey(Meal, on_delete=models.CASCADE)
    product = models.ForeignKey(Product, null=True, blank=True, on_delete=models.SET_NULL)
    dish = models.ForeignKey(Dish, null=True, blank=True, on_delete=models.SET_NULL)
    quantity = models.FloatField()

    def __str__(self):
        return f"{self.meal.date} - {self.meal.meal_type} - {self.product.name if self.product else self.dish.name}"

class FitnessActivity(models.Model):
    date = models.DateField()
    activity_type = models.CharField(max_length=255)
    duration = models.DurationField()
    calories_burned = models.FloatField()

    def __str__(self):
        return f"{self.date} - {self.activity_type}"

class UserGoal(models.Model):
    GOAL_TYPES = [
        ('weight', 'Вага'),
        ('calories', 'Калорії'),
        ('activity', 'Активність'),
    ]

    goal_type = models.CharField(max_length=10, choices=GOAL_TYPES)
    target_value = models.FloatField()
    start_date = models.DateField()
    end_date = models.DateField()

    def __str__(self):
        return f"{self.goal_type} - {self.target_value}"

class UserProfile(models.Model):
    GENDER_CHOICES = [
        ('male', 'Чоловік'),
        ('female', 'Жінка'),
    ]

    ACTIVITY_LEVELS = [
        ('sedentary', 'Малорухливий'),
        ('lightly_active', 'Легка активність'),
        ('moderately_active', 'Помірна активність'),
        ('very_active', 'Висока активність'),
        ('extra_active', 'Екстра активність'),
    ]

    GOAL_CHOICES = [
        ('lose_weight', 'Схуднення'),
        ('gain_weight', 'Набір маси'),
        ('maintain_weight', 'Підтримка ваги'),
    ]

    age = models.IntegerField()
    gender = models.CharField(max_length=10, choices=GENDER_CHOICES)
    height = models.FloatField()
    weight = models.FloatField()
    activity_level = models.CharField(max_length=20, choices=ACTIVITY_LEVELS)
    goal = models.CharField(max_length=20, choices=GOAL_CHOICES)

    def __str__(self):
        return f"{self.age} - {self.gender} - {self.goal}"

```