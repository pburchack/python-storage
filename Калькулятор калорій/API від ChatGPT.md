Ось приклад реалізації простого API для роботи з базою даних у Django за допомогою Django REST Framework. Ми створимо серіалізатори та представлення для моделей, які ви вказали. 

Спершу переконайтеся, що у вас встановлений Django REST Framework. Додайте його до `INSTALLED_APPS` у вашому файлі `settings.py`:

```python
INSTALLED_APPS = [
    ...
    'rest_framework',
    ...
]
```

Тепер створимо файл `serializers.py` для серіалізації даних:

### `serializers.py`

```python
from rest_framework import serializers
from .models import Product, Dish, DishProduct, Meal, MealItem, FitnessActivity, UserGoal, UserProfile

class ProductSerializer(serializers.ModelSerializer):
    """
    Серіалізатор для моделі Product.
    """
    class Meta:
        model = Product
        fields = '__all__'  # Включає всі поля моделі

class DishSerializer(serializers.ModelSerializer):
    """
    Серіалізатор для моделі Dish.
    """
    class Meta:
        model = Dish
        fields = '__all__'  # Включає всі поля моделі

class DishProductSerializer(serializers.ModelSerializer):
    """
    Серіалізатор для моделі DishProduct.
    """
    class Meta:
        model = DishProduct
        fields = '__all__'  # Включає всі поля моделі

class MealSerializer(serializers.ModelSerializer):
    """
    Серіалізатор для моделі Meal.
    """
    class Meta:
        model = Meal
        fields = '__all__'  # Включає всі поля моделі

class MealItemSerializer(serializers.ModelSerializer):
    """
    Серіалізатор для моделі MealItem.
    """
    class Meta:
        model = MealItem
        fields = '__all__'  # Включає всі поля моделі

class FitnessActivitySerializer(serializers.ModelSerializer):
    """
    Серіалізатор для моделі FitnessActivity.
    """
    class Meta:
        model = FitnessActivity
        fields = '__all__'  # Включає всі поля моделі

class UserGoalSerializer(serializers.ModelSerializer):
    """
    Серіалізатор для моделі UserGoal.
    """
    class Meta:
        model = UserGoal
        fields = '__all__'  # Включає всі поля моделі

class UserProfileSerializer(serializers.ModelSerializer):
    """
    Серіалізатор для моделі UserProfile.
    """
    class Meta:
        model = UserProfile
        fields = '__all__'  # Включає всі поля моделі
```

### `views.py`

Тепер створимо представлення для роботи з API:

```python
from rest_framework import viewsets
from .models import Product, Dish, DishProduct, Meal, MealItem, FitnessActivity, UserGoal, UserProfile
from .serializers import (
    ProductSerializer,
    DishSerializer,
    DishProductSerializer,
    MealSerializer,
    MealItemSerializer,
    FitnessActivitySerializer,
    UserGoalSerializer,
    UserProfileSerializer
)

class ProductViewSet(viewsets.ModelViewSet):
    """
    Представлення для роботи з продуктами.
    """
    queryset = Product.objects.all()
    serializer_class = ProductSerializer

class DishViewSet(viewsets.ModelViewSet):
    """
    Представлення для роботи зі стравами.
    """
    queryset = Dish.objects.all()
    serializer_class = DishSerializer

class DishProductViewSet(viewsets.ModelViewSet):
    """
    Представлення для роботи зі зв'язками страви та продуктів.
    """
    queryset = DishProduct.objects.all()
    serializer_class = DishProductSerializer

class MealViewSet(viewsets.ModelViewSet):
    """
    Представлення для роботи з прийомами їжі.
    """
    queryset = Meal.objects.all()
    serializer_class = MealSerializer

class MealItemViewSet(viewsets.ModelViewSet):
    """
    Представлення для роботи з елементами прийомів їжі.
    """
    queryset = MealItem.objects.all()
    serializer_class = MealItemSerializer

class FitnessActivityViewSet(viewsets.ModelViewSet):
    """
    Представлення для роботи з фізичною активністю.
    """
    queryset = FitnessActivity.objects.all()
    serializer_class = FitnessActivitySerializer

class UserGoalViewSet(viewsets.ModelViewSet):
    """
    Представлення для роботи з цілями користувача.
    """
    queryset = UserGoal.objects.all()
    serializer_class = UserGoalSerializer

class UserProfileViewSet(viewsets.ModelViewSet):
    """
    Представлення для роботи з профілем користувача.
    """
    queryset = UserProfile.objects.all()
    serializer_class = UserProfileSerializer
```

### `urls.py`

Тепер потрібно додати URL-адреси для цих представлень. Створіть або оновіть файл `urls.py` у вашому додатку:

```python
from django.urls import path, include
from rest_framework.routers import DefaultRouter
from .views import (
    ProductViewSet,
    DishViewSet,
    DishProductViewSet,
    MealViewSet,
    MealItemViewSet,
    FitnessActivityViewSet,
    UserGoalViewSet,
    UserProfileViewSet
)

# Створюємо маршрутизатор
router = DefaultRouter()
router.register(r'products', ProductViewSet)
router.register(r'dishes', DishViewSet)
router.register(r'dish-products', DishProductViewSet)
router.register(r'meals', MealViewSet)
router.register(r'meal-items', MealItemViewSet)
router.register(r'fitness-activities', FitnessActivityViewSet)
router.register(r'user-goals', UserGoalViewSet)
router.register(r'user-profiles', UserProfileViewSet)

urlpatterns = [
    path('', include(router.urls)),  # Включаємо маршрути з маршрутизатора
]
```

### Тестування API

Тепер у вас є повноцінний API для вашого проекту. Ви можете використовувати інструменти, такі як Postman або curl, щоб тестувати ваш API. Django REST Framework автоматично генерує інтерфейс для тестування, якщо ви перейдете на `/admin` або `/api-docs`.

Не забудьте налаштувати базу даних у `settings.py` та запустити міграції:

```bash
python manage.py makemigrations
python manage.py migrate
```

Цей код забезпечить основну функціональність API для роботи з вашою базою даних. Ви можете доповнювати його додатковими можливостями відповідно до ваших потреб.