Звісно, я допоможу вам створити API для роботи з базою даних. Для цього ми використаємо Django Rest Framework (DRF). Ось покроковий план:

1. Встановіть Django Rest Framework:
```
pip install djangorestframework
```

2. Додайте 'rest_framework' до INSTALLED_APPS у файлі settings.py.

3. Створіть файл serializers.py у вашому додатку:

```python
from rest_framework import serializers
from .models import Product, Dish, Meal, MealItem, FitnessActivity, UserGoal, UserProfile

class ProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = '__all__'

class DishSerializer(serializers.ModelSerializer):
    class Meta:
        model = Dish
        fields = '__all__'

class MealSerializer(serializers.ModelSerializer):
    class Meta:
        model = Meal
        fields = '__all__'

class MealItemSerializer(serializers.ModelSerializer):
    class Meta:
        model = MealItem
        fields = '__all__'

class FitnessActivitySerializer(serializers.ModelSerializer):
    class Meta:
        model = FitnessActivity
        fields = '__all__'

class UserGoalSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserGoal
        fields = '__all__'

class UserProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserProfile
        fields = '__all__'
```

4. Створіть файл views.py або оновіть існуючий:

```python
from rest_framework import viewsets
from .models import Product, Dish, Meal, MealItem, FitnessActivity, UserGoal, UserProfile
from .serializers import ProductSerializer, DishSerializer, MealSerializer, MealItemSerializer, FitnessActivitySerializer, UserGoalSerializer, UserProfileSerializer

class ProductViewSet(viewsets.ModelViewSet):
    queryset = Product.objects.all()
    serializer_class = ProductSerializer

class DishViewSet(viewsets.ModelViewSet):
    queryset = Dish.objects.all()
    serializer_class = DishSerializer

class MealViewSet(viewsets.ModelViewSet):
    queryset = Meal.objects.all()
    serializer_class = MealSerializer

class MealItemViewSet(viewsets.ModelViewSet):
    queryset = MealItem.objects.all()
    serializer_class = MealItemSerializer

class FitnessActivityViewSet(viewsets.ModelViewSet):
    queryset = FitnessActivity.objects.all()
    serializer_class = FitnessActivitySerializer

class UserGoalViewSet(viewsets.ModelViewSet):
    queryset = UserGoal.objects.all()
    serializer_class = UserGoalSerializer

class UserProfileViewSet(viewsets.ModelViewSet):
    queryset = UserProfile.objects.all()
    serializer_class = UserProfileSerializer
```

5. Оновіть файл urls.py вашого проекту:

```python
from django.contrib import admin
from django.urls import path, include
from rest_framework.routers import DefaultRouter
from yourapp.views import ProductViewSet, DishViewSet, MealViewSet, MealItemViewSet, FitnessActivityViewSet, UserGoalViewSet, UserProfileViewSet

router = DefaultRouter()
router.register(r'products', ProductViewSet)
router.register(r'dishes', DishViewSet)
router.register(r'meals', MealViewSet)
router.register(r'mealitems', MealItemViewSet)
router.register(r'fitnessactivities', FitnessActivityViewSet)
router.register(r'usergoals', UserGoalViewSet)
router.register(r'userprofiles', UserProfileViewSet)

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/', include(router.urls)),
]
```

Цей код створює повний CRUD API для кожної моделі. Ви зможете виконувати наступні операції:

- GET /api/products/ - отримати список всіх продуктів
- POST /api/products/ - створити новий продукт
- GET /api/products/{id}/ - отримати деталі конкретного продукту
- PUT /api/products/{id}/ - оновити продукт
- DELETE /api/products/{id}/ - видалити продукт

Аналогічні операції доступні для інших моделей (dishes, meals, mealitems, fitnessactivities, usergoals, userprofiles).

Для тестування API ви можете використовувати інструменти, такі як Postman, або просто відкрити ці URL у браузері (для GET-запитів).

Не забудьте налаштувати відповідні дозволи та аутентифікацію, якщо це необхідно для вашого додатку.

Якщо вам потрібні додаткові ендпоінти або специфічна логіка, ви можете розширити ці ViewSet'и або створити додаткові представлення.