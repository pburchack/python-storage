Звісно! Давайте створимо API для роботи з базою даних за допомогою Django REST Framework (DRF). Спочатку переконайтеся, що у вас встановлено DRF:

```bash
pip install djangorestframework
```

Потім додайте `rest_framework` до `INSTALLED_APPS` у вашому файлі `settings.py`:

```python
INSTALLED_APPS = [
    ...
    'rest_framework',
    ...
]
```

Теперь створимо серіалізатори, представлення та маршрути для нашого API.

### Серіалізатори (serializers.py)

Створіть файл `serializers.py` у вашому додатку:

```python
from rest_framework import serializers
from .models import Product, Dish, DishProduct, Meal, MealItem, FitnessActivity, UserGoal, UserProfile

class ProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = '__all__'

class DishSerializer(serializers.ModelSerializer):
    class Meta:
        model = Dish
        fields = '__all__'

class DishProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = DishProduct
        fields = '__all__'

class MealSerializer(serializers.ModelSerializer):
    class Meta:
        model = Meal
        fields = '__all__'

class MealItemSerializer(serializers.ModelSerializer):
    class Meta:
        model = MealItem
        fields = '__all__'

class FitnessActivitySerializer(serializers.ModelSerializer):
    class Meta:
        model = FitnessActivity
        fields = '__all__'

class UserGoalSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserGoal
        fields = '__all__'

class UserProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserProfile
        fields = '__all__'
```

### Представлення (views.py)

Створіть файл `views.py` у вашому додатку:

```python
from rest_framework import viewsets
from .models import Product, Dish, DishProduct, Meal, MealItem, FitnessActivity, UserGoal, UserProfile
from .serializers import (
    ProductSerializer, DishSerializer, DishProductSerializer,
    MealSerializer, MealItemSerializer, FitnessActivitySerializer,
    UserGoalSerializer, UserProfileSerializer
)

class ProductViewSet(viewsets.ModelViewSet):
    queryset = Product.objects.all()
    serializer_class = ProductSerializer

class DishViewSet(viewsets.ModelViewSet):
    queryset = Dish.objects.all()
    serializer_class = DishSerializer

class DishProductViewSet(viewsets.ModelViewSet):
    queryset = DishProduct.objects.all()
    serializer_class = DishProductSerializer

class MealViewSet(viewsets.ModelViewSet):
    queryset = Meal.objects.all()
    serializer_class = MealSerializer

class MealItemViewSet(viewsets.ModelViewSet):
    queryset = MealItem.objects.all()
    serializer_class = MealItemSerializer

class FitnessActivityViewSet(viewsets.ModelViewSet):
    queryset = FitnessActivity.objects.all()
    serializer_class = FitnessActivitySerializer

class UserGoalViewSet(viewsets.ModelViewSet):
    queryset = UserGoal.objects.all()
    serializer_class = UserGoalSerializer

class UserProfileViewSet(viewsets.ModelViewSet):
    queryset = UserProfile.objects.all()
    serializer_class = UserProfileSerializer
```

### Маршрути (urls.py)

Створіть файл `urls.py` у вашому додатку:

```python
from django.urls import path, include
from rest_framework.routers import DefaultRouter
from .views import (
    ProductViewSet, DishViewSet, DishProductViewSet,
    MealViewSet, MealItemViewSet, FitnessActivityViewSet,
    UserGoalViewSet, UserProfileViewSet
)

router = DefaultRouter()
router.register(r'products', ProductViewSet)
router.register(r'dishes', DishViewSet)
router.register(r'dishproducts', DishProductViewSet)
router.register(r'meals', MealViewSet)
router.register(r'mealitems', MealItemViewSet)
router.register(r'fitnessactivities', FitnessActivityViewSet)
router.register(r'usergoals', UserGoalViewSet)
router.register(r'userprofiles', UserProfileViewSet)

urlpatterns = [
    path('', include(router.urls)),
]
```

### Основний файл маршрутів (urls.py)

Переконайтеся, що ваш основний файл `urls.py` у проекті включає маршрути вашого додатку:

```python
from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/', include('your_app_name.urls')),  # Замініть 'your_app_name' на ім'я вашого додатку
]
```

Тепер у вас є базовий API для роботи з вашою базою даних. Ви можете використовувати Django REST Framework для створення, читання, оновлення та видалення записів у вашій базі даних через API.