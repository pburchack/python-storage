```Python
from django.db import models

class Product(models.Model):
    """
    Модель для представлення продукту.
    """
    id = models.AutoField(primary_key=True)  # Унікальний ідентифікатор продукту
    name = models.CharField(max_length=255)  # Назва продукту
    calories = models.FloatField()  # Кількість калорій
    proteins = models.FloatField()  # Кількість білків
    fats = models.FloatField()  # Кількість жирів
    carbohydrates = models.FloatField()  # Кількість вуглеводів
    fiber = models.FloatField()  # Кількість клітковини
    image = models.ImageField(upload_to='product_images/', null=True, blank=True)  # Зображення продукту

    def __str__(self):
        return self.name

class Dish(models.Model):
    """
    Модель для представлення страви.
    """
    id = models.AutoField(primary_key=True)  # Унікальний ідентифікатор страви
    name = models.CharField(max_length=255)  # Назва страви
    instructions = models.TextField()  # Інструкції з приготування

    def __str__(self):
        return self.name

class DishProduct(models.Model):
    """
    Модель для зв'язку між стравами та продуктами.
    """
    id = models.AutoField(primary_key=True)  # Унікальний ідентифікатор зв'язку
    dish = models.ForeignKey(Dish, on_delete=models.CASCADE)  # Посилання на страву
    product = models.ForeignKey(Product, on_delete=models.CASCADE)  # Посилання на продукт
    quantity = models.FloatField()  # Кількість продукту в страві

class Meal(models.Model):
    """
    Модель для представлення прийому їжі.
    """
    id = models.AutoField(primary_key=True)  # Унікальний ідентифікатор прийому їжі
    date = models.DateField()  # Дата прийому їжі
    meal_type = models.CharField(max_length=20, choices=[
        ('breakfast', 'Сніданок'),
        ('lunch', 'Обід'),
        ('dinner', 'Вечеря'),
        ('snack', 'Перекус')
    ])  # Тип прийому їжі

class MealItem(models.Model):
    """
    Модель для елемента прийому їжі.
    """
    id = models.AutoField(primary_key=True)  # Унікальний ідентифікатор елемента
    meal = models.ForeignKey(Meal, on_delete=models.CASCADE)  # Посилання на прийом їжі
    product = models.ForeignKey(Product, null=True, blank=True, on_delete=models.CASCADE)  # Посилання на продукт (необов'язково)
    dish = models.ForeignKey(Dish, null=True, blank=True, on_delete=models.CASCADE)  # Посилання на страву (необов'язково)
    quantity = models.FloatField()  # Кількість продукту або страви

class FitnessActivity(models.Model):
    """
    Модель для представлення фізичної активності.
    """
    id = models.AutoField(primary_key=True)  # Унікальний ідентифікатор активності
    date = models.DateField()  # Дата фізичної активності
    activity_type = models.CharField(max_length=255)  # Тип активності
    duration = models.DurationField()  # Тривалість активності
    calories_burned = models.FloatField()  # Кількість спалених калорій

class UserGoal(models.Model):
    """
    Модель для представлення цілей користувача.
    """
    id = models.AutoField(primary_key=True)  # Унікальний ідентифікатор цілі
    goal_type = models.CharField(max_length=20, choices=[
        ('weight', 'Вага'),
        ('calories', 'Калорії'),
        ('activity', 'Активність')
    ])  # Тип цілі
    target_value = models.FloatField()  # Цільове значення
    start_date = models.DateField()  # Дата початку
    end_date = models.DateField()  # Дата завершення

class UserProfile(models.Model):
    """
    Модель для представлення профілю користувача.
    """
    id = models.AutoField(primary_key=True)  # Унікальний ідентифікатор профілю
    age = models.IntegerField()  # Вік
    gender = models.CharField(max_length=10, choices=[
        ('male', 'Чоловік'),
        ('female', 'Жінка'),
        ('other', 'Інше')
    ])  # Стать
    height = models.FloatField()  # Зріст
    weight = models.FloatField()  # Вага
    activity_level = models.CharField(max_length=20)  # Рівень активності
    goal = models.CharField(max_length=50)  # Ціль (наприклад, схуднення, набір маси)

    def __str__(self):
        return f"Profile of {self.gender}, Age: {self.age}"

```