Встановлюємо самбу
```bash
sudo apt install samba
```
Створюємо каталог для шарінгу (приклад для каталогу в корені диску)
```bash
sudo mkdir /shara/
```
Можна створити підкаталоги
```bash
sudo mkdir /shara/111
sudo mkdir /shara/22
sudo mkdir /shara/3
```
Даємо права каталогам рекурсивно
```bash
sudo chmod -R 0755 /shara/
```

Встановлюємо овнера каталогів рекурсивно.

```bash
sudo chown -R nobody:nogroup /shara/
```
в /etc/samba/smb.conf

створюємо секцію
```
[shara]
   path = /shara/
   browsabl = yes
   writable = yes
   guest ok = yes
   read only = no
```

Доступ з віндових компів без паролю. за ІР адресом (\\192.168.1.12\shara).
якщо адрес динамічний зручно підключити мережевий диск.