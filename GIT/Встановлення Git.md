Встановлення Git може відрізнятися в залежності від операційної системи, але загальний процес досить схожий. Ось кроки для встановлення Git на різних операційних системах:

### Windows

1. **Завантажте інсталятор Git**: Відвідайте офіційний сайт Git (https://git-scm.com/) та завантажте останню версію Git для Windows.
   
2. **Запустіть інсталятор**: Після завантаження запустіть інсталятор Git та слідуйте інструкціям у майстері інсталяції. Зазвичай ви можете прийняти всі налаштування за замовчуванням.

3. **Перевірте встановлення**: Після завершення інсталяції відкрийте командний рядок (Command Prompt) або термінал та введіть команду `git --version`, щоб перевірити, чи встановлено Git та його версію.

### macOS

1. **Використовуйте Homebrew (рекомендовано)**: Якщо у вас встановлено Homebrew, ви можете встановити Git, виконавши команду `brew install git` у терміналі.

2. **Використовуйте інсталятор Git**: Відвідайте офіційний сайт Git (https://git-scm.com/) та завантажте інсталятор Git для macOS. Відкрийте завантажений файл та слідуйте інструкціям майстра інсталяції.

3. **Перевірте встановлення**: Відкрийте термінал та введіть команду `git --version`, щоб перевірити, чи встановлено Git та його версію.

### Linux (Ubuntu)

1. **Використовуйте пакетний менеджер**: Відкрийте термінал та введіть команду:
   ```
   sudo apt update
   sudo apt install git
   ```

2. **Перевірте встановлення**: Після завершення встановлення введіть команду `git --version`, щоб перевірити, чи встановлено Git та його версію.

Після встановлення Git можна налаштувати ім'я та email для використання при комітуванні змін, використовуючи команди `git config --global user.name "Ваше ім'я"` та `git config --global user.email "ваша_емейл@пошта.com"`.