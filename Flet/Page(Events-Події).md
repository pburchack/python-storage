**on_app_lifecycle_state_change**
Активується, коли змінюється стан життєвого циклу програми.
Ви можете використовувати цю подію, щоб дізнатися, коли програма стане активною (перенесена на передній план), щоб оновити інтерфейс користувача останньою інформацією. Ця подія працює на iOS, Android, усіх настільних платформах і в Інтернеті.
Аргумент обробника подій має тип AppLifecycleStateChangeEvent.
[AppLifecycleStateChangeEvent | Flet](https://flet.dev/docs/reference/types/applifecyclestatechangeevent/)
