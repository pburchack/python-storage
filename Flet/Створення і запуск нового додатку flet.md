[[Flet]] 
Створюємо [[venv]]
Інсталимо Flet
```Shell
pip install flet
```
Створюєvо Flet додаток
```Shell
flet create <project-name>
```
Щоб створити програму Flet у поточному каталозі, виконайте таку команду:
```Shell
flet create .
```
Запуск додатку
```Shell
flet run
```
Запустить файл **main.py** в поточному каталозі
Якщо вам потрібно вказати інший шлях до файлу, скористайтеся такою командою:
```Shell
flet run <script>
```
Щоб запустити **main.py**, який знаходиться в іншому каталозі, вкажіть абсолютний або відносний шлях до каталогу, де він розташований, наприклад:
```Shell
flet run /Users/JohnSmith/Documents/projects/flet-app
```
Щоб запустити програму Flet як веб-програму, скористайтеся такою командою:
```Shell
flet run --web <script>
```
Відкриється нове вікно/вкладка браузера, і програма використовуватиме випадковий порт TCP
Для запуску на фіксованому порту використовуйте параметр --port (-p), наприклад:
```Shell
flet run --web --port 8000 app.py
```

Повна документація команди **run**
```Shell
usage: flet run [-h] [-v] [-p PORT] [--host HOST] [--name APP_NAME] [-m] [-d] [-r] [-n] [-w] [--ios] [--android] [-a ASSETS_DIR] [--ignore-dirs IGNORE_DIRS] [script]

Run Flet app.

positional arguments:
  script                path to a Python script

options:
  -h, --help            show this help message and exit
  -v, --verbose         -v for detailed output and -vv for more detailed
  -p PORT, --port PORT  custom TCP port to run Flet app on
  --host HOST           host to run Flet web app on. Use "*" to listen on all IPs.
  --name APP_NAME       app name to distinguish it from other on the same port
  -m, --module          treat the script as a python module path as opposed to a file path
  -d, --directory       watch script directory
  -r, --recursive       watch script directory and all sub-directories recursively
  -n, --hidden          application window is hidden on startup
  -w, --web             open app in a web browser
  --ios                 open app on iOS device
  --android             open app on Android device
  -a ASSETS_DIR, --assets ASSETS_DIR
                        path to assets directory
  --ignore-dirs IGNORE_DIRS
                        directories to ignore during watch. If more than one, separate with a comma.
```