[Flet documentation(Custom controls | Flet](https://flet.dev/docs/getting-started/custom-controls))
Хоча Flet надає понад 100 вбудованих елементів керування, які можна використовувати окремо, справжня краса програмування з Flet полягає в тому, що всі ці елементи керування можна використовувати для створення власних багаторазових компонентів інтерфейсу користувача за допомогою концепцій об’єктно-орієнтованого програмування Python.
Ви можете створювати власні елементи керування в Python, додаючи стилі та/або комбінуючи існуючі елементи керування Flet.
## Styled controls
Найпростіший настроюваний елемент керування, який ви можете створити, — це стильовий елемент керування, наприклад кнопка певного кольору та поведінки, яка використовуватиметься кілька разів у вашій програмі.
Щоб створити стильовий елемент керування, вам потрібно створити новий клас у Python, який успадковує елемент керування Flet, який ви збираєтеся налаштувати, `ElevatedButton` у цьому випадку:
```Python
class MyButton(ft.ElevatedButton):
    def __init__(self, text):
        super().__init__()
        self.bgcolor = ft.colors.ORANGE_300
        self.color = ft.colors.GREEN_800
        self.text = text     
```
У вашому елементі керування є конструктор для налаштування властивостей і подій і передачі користувацьких даних. Зауважте, що ви повинні викликати `super().__init__()` у своєму власному конструкторі, щоб отримати доступ до властивостей і методів елемента керування Flet, від якого ви успадкували.
Тепер ви можете використовувати свій абсолютно новий елемент керування у своїй програмі:
```Python
import flet as ft

def main(page: ft.Page):

    page.add(MyButton(text="OK"), MyButton(text="Cancel"))

ft.app(target=main)
```
### Handling events
Подібно до властивостей, ви можете передавати обробники подій як параметри у свій власний конструктор класу керування:
```Python
import flet as ft

class MyButton(ft.ElevatedButton):
    def __init__(self, text, on_click):
        super().__init__()
        self.bgcolor = ft.colors.ORANGE_300
        self.color = ft.colors.GREEN_800
        self.text = text
        self.on_click = on_click

def main(page: ft.Page):

    def ok_clicked(e):
        print("OK clicked")
    
    def cancel_clicked(e):
        print("Cancel clicked")

    page.add(
        MyButton(text="OK", on_click=ok_clicked),
        MyButton(text="Cancel", on_click=cancel_clicked),
    )

ft.app(target=main)
```
## Composite controls
Складені настроювані елементи керування успадковуються від елементів керування контейнера, таких як `Column`, `Row`, `Stack` або навіть `view`, щоб об’єднати кілька елементів керування Flet. У наведеному нижче прикладі показано елемент керування завданням, який можна використовувати в програмі To-Do:
```Python
import flet as ft
class Task(ft.Row):
    def __init__(self, text):
        super().__init__()
        self.text_view = ft.Text(text)
        self.text_edit = ft.TextField(text, visible=False)
        self.edit_button = ft.IconButton(icon=ft.icons.EDIT, on_click=self.edit)
        self.save_button = ft.IconButton(
            visible=False, icon=ft.icons.SAVE, on_click=self.save
        )
        self.controls = [
            ft.Checkbox(),
            self.text_view,
            self.text_edit,
            self.edit_button,
            self.save_button,
        ]

    def edit(self, e):
        self.edit_button.visible = False
        self.save_button.visible = True
        self.text_view.visible = False
        self.text_edit.visible = True
        self.update()

    def save(self, e):
        self.edit_button.visible = True
        self.save_button.visible = False
        self.text_view.visible = True
        self.text_edit.visible = False
        self.text_view.value = self.text_edit.value
        self.update()

def main(page: ft.Page):

    page.add(
        Task(text="Do laundry"),
        Task(text="Cook dinner"),
    )


ft.app(target=main)
```
## Life-cycle methods
Настроювані елементи керування надають методи «перехоплення» життєвого циклу, які вам, можливо, доведеться використовувати для різних випадків використання у вашій програмі.

### `build()`
Метод `build()` викликається, коли елемент керування створюється та призначається йому `self.page`.
Перевизначте метод `build()`, якщо вам потрібно реалізувати логіку, яку неможливо виконати в конструкторі елемента керування, оскільки для цього потрібен доступ до `self.page`. Наприклад, виберіть правильний значок залежно від `self.page.platform` для вашої адаптивної програми.
### `did_mount()`
Метод `did_mount()` викликається після того, як елемент керування додано до сторінки та призначено тимчасовий uid.
Замініть метод `did_mount()`, якщо вам потрібно реалізувати логіку, яку потрібно виконати після того, як елемент керування було додано на сторінку, наприклад, віджет погоди, який щохвилини викликає `Open Weather API`, щоб оновлюватися з новими погодними умовами.
### `will_unmount()`
Метод `will_unmount()` викликається перед видаленням елемента керування зі сторінки.
Перевизначте метод `will_unmount()` для виконання коду очищення.
### `before_update()`
Метод `before_update()` викликається щоразу, коли оновлюється елемент керування.
Переконайтеся, що не викликаєте метод `update()` у `before_update()`.

## Isolated controls
Спеціальний елемент керування має властивість` is_isolated`, яка за умовчанням має значення `False`.
Якщо для параметра `is_isolated` встановлено значення `True`, ваш елемент керування буде ізольовано від зовнішнього макета, тобто коли метод `update()` викликається для батьківського елемента керування, сам елемент керування буде оновлено, але будь-які зміни дочірніх елементів керування не включено до дайджесту оновлення . Ізольовані елементи керування мають викликати `self.update()`, щоб перенести свої зміни на сторінку Flet.
Як найкраща практика, будь-який спеціальний елемент керування, який викликає `self.update()` у своїх методах класу, має бути ізольованим.