[Page | Flet](https://flet.dev/docs/controls/page/)
**Сторінка** – це контейнер для елементів керування.
Екземпляр сторінки та кореневий перегляд створюються автоматично, коли починається новий сеанс користувача.



**[[appbar]]** - стрічка аплікації яку можна додати на сторінку
[AppBar | Flet](https://flet.dev/docs/controls/appbar/)
![](https://flet.dev/img/docs/controls/app-bar/app-bar.gif)

**bgcolor**
Колір фону сторінки.
Значення кольору може бути шістнадцятковим у форматі \#RGB(наприклад, \#FFCC0000), \#RGB (наприклад, \#CC0000) або іменованим кольором із модуля [[flet.colors]]

**[[bottom_appbar]]**
Елемент керування BottomAppBar для відображення внизу сторінки. Якщо надано властивості bottom_appbar і navigation_bar, буде відображено NavigationBar.
[BottomAppBar | Flet](https://flet.dev/docs/controls/bottomappbar/)
![](https://flet.dev/img/docs/controls/bottom-app-bar/bottom-app-bar.png)

**browser_context_menu**
Використовується для ввімкнення або вимкнення контекстного меню, яке з’являється, коли користувач клацає правою кнопкою миші на веб-сторінці.
[BrowserContextMenu | Flet](https://flet.dev/docs/reference/types/browsercontextmenu/)
*тільки для WEB*

**client_ip**
IP-адреса підключеного користувача.

**client_user_agent**
Відомості про браузер підключеного користувача.

**controls**
Список елементів керування для відображення на сторінці.
```Python
    page.controls.extend([menappbar, label, footer_bottom_appbar])
    page.controls.append(ft.Text("Hello!"))
    page.update()
```
або можна отримати той самий результат, що й вище, використовуючи метод **page.add()**.
```Python
	page.add(menappbar, label, footer_bottom_appbar)
	page.update()
```
Щоб видалити самий останній елемент керування на сторінці:
```Python
	page.controls.pop()  
	page.update()
```

**drawer**
Елемент керування NavigationDrawer для відображення як панель, що ковзає від початкового краю сторінки.
[NavigationDrawer | Flet](https://flet.dev/docs/controls/navigationdrawer/)
![](https://flet.dev/img/docs/controls/navigationdrawer/navigation-drawer-start.gif)

**floating_action_button**
Елемент керування FloatingActionButton для відображення поверх вмісту сторінки.
[FloatingActionButton | Flet](https://flet.dev/docs/controls/floatingactionbutton/)
![](https://flet.dev/img/docs/controls/floatingactionbutton/custom-fab.gif)

**fonts**
Дозволяє імпортувати спеціальні шрифти та використовувати їх із Text.font_family або застосовувати до всієї програми через theme.font_family.

**horizontal_alignment**
Горизонтальне вирівнювання. Можливі значення (START, CENTER, END, STRETH. BASELINE)

```Python
page.horizontal_alignment = ft.CrossAxisAlignment.END
```
**NavigationBar**
Елемент керування NavigationBar для відображення внизу сторінки. Якщо надано властивості bottom_appbar і navigation_bar, буде відображено NavigationBar.
[NavigationBar | Flet](https://flet.dev/docs/controls/navigationbar/)
![](https://flet.dev/img/docs/controls/navigation-bar/navigation-bar-sample.gif)

**padding**
Відстань між вмістом сторінки та її краями. Стандартне значення становить 10 пікселів з кожного боку
[Padding | Flet](https://flet.dev/docs/reference/types/padding/)

**scroll**
Вмикає вертикальне прокручування сторінки, щоб запобігти переповненню вмісту.
[ScrollMode | Flet](https://flet.dev/docs/reference/types/scrollmode/)
```Python
    page.scroll = ft.ScrollMode.AUTO
```

**spacing**
Вертикальний інтервал між елементами керування на сторінці. Значення за замовчуванням — 10 віртуальних пікселів. Інтервал застосовується лише тоді, коли вирівнювання встановлено на початок, кінець або центр.

**theme_mode**
Тематичний режим сторінки.
Значення має тип ThemeMode і за замовчуванням — ThemeMode.SYSTEM.
Можна також встановити в LIGHT or DARK

```Python
    page.theme_mode = 'DARK'
```

**title**
Заголовок вікна

**url**
Повна URL-адреса веб-програми.

```Python
    url = page.url
    print(url)
```

**vertical_alignment**
Вертикальне вирівнювання
Значення має тип MainAxisAlignment і за замовчуванням — MainAxisAlignment.START.
[MainAxisAlignment | Flet](https://flet.dev/docs/reference/types/mainaxisalignment/)
```Python
	page.vertical_alignment = ft.MainAxisAlignment.CENTER
```