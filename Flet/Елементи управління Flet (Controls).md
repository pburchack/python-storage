[Flet documentation](https://flet.dev/docs/getting-started/flet-controls)
Інтерфейс користувача складається з елементів керування (також віджетів). Щоб зробити елементи керування видимими для користувача, їх потрібно додати на сторінку або в інші елементи керування. Сторінка є верхнім елементом керування. Вкладені елементи керування один в одного можна представити у вигляді дерева зі сторінкою як коренем.
Елементи керування — це звичайні класи Python. Створіть екземпляри керування за допомогою конструкторів із параметрами, що відповідають їхнім властивостям, наприклад:
```Python
t = ft.Text(value="Hello, world!", color="green")
```
Щоб відобразити елемент керування на сторінці, додайте його до списку елементів керування сторінки та викличте page.update(), щоб надіслати зміни сторінки в браузер або настільний клієнт:
```Python
import flet as ft

def main(page: ft.Page):
    t = ft.Text(value="Hello, world!", color="green")
    page.controls.append(t)
    page.update()

ft.app(target=main)
```
Ви можете змінити властивості елемента керування, і інтерфейс користувача буде оновлено з допомогою `page.update()`:
```Python
t = ft.Text()  
page.add(t) # it's a shortcut for page.controls.append(t) and then page.update()  
  
for i in range(10):  
t.value = f"Step {i}"  
page.update()  
time.sleep(1)
```
Деякі елементи керування є «контейнерними» елементами керування (наприклад, Page), які можуть містити інші елементи керування. Наприклад, елемент керування Row дозволяє впорядкувати інші елементи керування в рядку один за одним:
```Python
page.add(
    ft.Row(controls=[
        ft.Text("A"),
        ft.Text("B"),
        ft.Text("C")
    ])
)
```
`page.update()` достатньо розумний, щоб надсилати лише зміни, внесені з моменту його останнього виклику, тому ви можете додати кілька нових елементів керування на сторінку, видалити деякі з них, змінити властивості інших елементів керування, а потім викликати `page.update()` щоб виконати пакетне оновлення, наприклад:
```Python
for i in range(10):
    page.controls.append(ft.Text(f"Line {i}"))
    if i > 4:
        page.controls.pop(0)
    page.update()
    time.sleep(0.3)
```
Деякі елементи керування, наприклад кнопки, можуть мати обробники подій, які реагують на введення користувача, наприклад `ElevatedButton.on_click`:
```Python
def button_clicked(e):
    page.add(ft.Text("Clicked!"))

page.add(ft.ElevatedButton(text="Click me", on_click=button_clicked))
```
**visible** властивість
Кожен елемент керування має `visible` властивість, яка за замовчуванням має значення `true` – елемент керування відображається на сторінці. Для параметра `visible` значення `false` повністю запобігає відображенню елемента керування (і всіх його дочірніх елементів, якщо такі є) на полотні сторінки. Приховані елементи керування не можна виділити за допомогою клавіатури чи миші, і вони не створюють жодних подій.
**disabled** властивість
Кожен елемент керування має властивість `disabled`, яка за замовчуванням має значення `false` – елемент керування та всі його дочірні елементи ввімкнено. властивість `disabled` здебільшого використовується з елементами керування введенням даних, такими як `TextField`, `Dropdown`, `Checkbox`, кнопки. Однак `disabled` можна встановити для батьківського елемента керування, і його значення рекурсивно поширюватиметься на всіх дочірніх елементів.
## Buttons
Кнопка є найважливішим елементом керування введенням, який генерує подію клацання під час натискання.
Усі події, згенеровані елементами керування на веб-сторінці, постійно надсилаються назад у ваш сценарій, тож як ви реагуєте на натискання кнопки?
### Event handlers
Кнопки з подіями в додатку «Лічильник»:
```Python
import flet as ft

def main(page: ft.Page):
    page.title = "Flet counter example"
    page.vertical_alignment = ft.MainAxisAlignment.CENTER

    txt_number = ft.TextField(value="0", text_align="right", width=100)

    def minus_click(e):
        txt_number.value = str(int(txt_number.value) - 1)
        page.update()

    def plus_click(e):
        txt_number.value = str(int(txt_number.value) + 1)
        page.update()

    page.add(
        ft.Row(
            [
                ft.IconButton(ft.icons.REMOVE, on_click=minus_click),
                txt_number,
                ft.IconButton(ft.icons.ADD, on_click=plus_click),
            ],
            alignment=ft.MainAxisAlignment.CENTER,
        )
    )

ft.app(target=main)
```