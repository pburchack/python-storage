@echo off
REM Встановлюємо кодування UTF-8
chcp 65001 >nul

REM Перевіряємо, чи знаходимося ми в Git репозиторії
git rev-parse --is-inside-work-tree >nul 2>&1
if errorlevel 1 (
    echo Це не Git репозиторій.
    exit /b 1
)

REM Додаємо всі зміни до індексу
echo Додаємо зміни до індексу...
git add .

REM Робимо коміт з повідомленням
set /p commitMessage="Введіть повідомлення для коміту: "
git commit -m "%commitMessage%"

REM Відправляємо зміни до віддаленого репозиторію
echo Відправляємо зміни до віддаленого репозиторію...
git push

echo Операція успішно завершена!
pause
