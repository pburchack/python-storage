Звісно, ось файл `models.py` на основі запропонованої схеми бази даних:

```python
from django.db import models
from django.contrib.auth.models import AbstractUser
from django.utils.translation import gettext_lazy as _

class User(AbstractUser):
    is_admin = models.BooleanField(default=False)
    created_at = models.DateTimeField(auto_now_add=True)

class Person(models.Model):
    GENDER_CHOICES = [
        ('M', _('Male')),
        ('F', _('Female')),
        ('O', _('Other')),
    ]
    
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    birth_date = models.DateField()
    death_date = models.DateField(null=True, blank=True)
    gender = models.CharField(max_length=1, choices=GENDER_CHOICES)
    bio = models.TextField(blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f"{self.first_name} {self.last_name}"

class Relationship(models.Model):
    RELATIONSHIP_CHOICES = [
        ('parent', _('Parent')),
        ('child', _('Child')),
        ('spouse', _('Spouse')),
    ]
    
    from_person = models.ForeignKey(Person, on_delete=models.CASCADE, related_name='relationships_from')
    to_person = models.ForeignKey(Person, on_delete=models.CASCADE, related_name='relationships_to')
    relationship_type = models.CharField(max_length=10, choices=RELATIONSHIP_CHOICES)
    start_date = models.DateField(null=True, blank=True)
    end_date = models.DateField(null=True, blank=True)

class Event(models.Model):
    EVENT_CHOICES = [
        ('birth', _('Birth')),
        ('death', _('Death')),
        ('marriage', _('Marriage')),
        ('divorce', _('Divorce')),
        ('other', _('Other')),
    ]
    
    person = models.ForeignKey(Person, on_delete=models.CASCADE, related_name='events')
    event_type = models.CharField(max_length=10, choices=EVENT_CHOICES)
    date = models.DateField()
    place = models.CharField(max_length=200)
    description = models.TextField(blank=True)

class Photo(models.Model):
    person = models.ForeignKey(Person, on_delete=models.CASCADE, related_name='photos')
    file_path = models.ImageField(upload_to='photos/')
    upload_date = models.DateTimeField(auto_now_add=True)
    description = models.TextField(blank=True)

class Document(models.Model):
    person = models.ForeignKey(Person, on_delete=models.CASCADE, related_name='documents')
    file_path = models.FileField(upload_to='documents/')
    document_type = models.CharField(max_length=100)
    issue_date = models.DateField()
    description = models.TextField(blank=True)

class Tree(models.Model):
    name = models.CharField(max_length=200)
    description = models.TextField(blank=True)
    owner = models.ForeignKey(User, on_delete=models.CASCADE, related_name='trees')
    is_public = models.BooleanField(default=False)
    created_at = models.DateTimeField(auto_now_add=True)

class TreeMember(models.Model):
    tree = models.ForeignKey(Tree, on_delete=models.CASCADE, related_name='members')
    person = models.ForeignKey(Person, on_delete=models.CASCADE)

    class Meta:
        unique_together = ('tree', 'person')
```

Цей файл `models.py` відображає структуру бази даних, яку ми обговорили раніше. Ось кілька важливих моментів:

1. Ми використовуємо `AbstractUser` для розширення стандартної моделі користувача Django.
2. Для полів з фіксованим набором значень (наприклад, `gender`, `relationship_type`, `event_type`) ми використовуємо `choices`.
3. Ми використовуємо `related_name` для зручності доступу до зв'язаних об'єктів.
4. Для файлових полів (`file_path` у `Photo` та `Document`) ми використовуємо `ImageField` та `FileField` відповідно.
5. У моделі `TreeMember` ми додали `unique_together` для забезпечення унікальності комбінації дерева та особи.

Не забудьте додати необхідні імпорти на початку файлу та налаштувати `settings.py` для використання кастомної моделі користувача:

```python
AUTH_USER_MODEL = 'yourapp.User'
```

Чи потрібні вам якісь додаткові пояснення або модифікації цієї моделі?