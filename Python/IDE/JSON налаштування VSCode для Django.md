Закоментовані налаштування діють тільки в Windows
### Global setting
```JSON
{

"extensions.autoCheckUpdates": false,

// "update.enableWindowsBackgroundUpdates": false,

"extensions.autoUpdate": false,

"update.showReleaseNotes": false,

"update.mode": "none",

  

"debug.inlineValues": "on",

"debug.autoExpandLazyVariables": true,

  

"editor.minimap.enabled": false,

"workbench.colorTheme": "Monokai",

"terminal.integrated.cursorStyle": "line",

"terminal.integrated.cursorBlinking": true,

"window.commandCenter": false,

"workbench.startupEditor": "none",

"files.autoSave": "afterDelay",

"explorer.autoReveal": false,

"explorer.compactFolders": false,

"breadcrumbs.enabled": false,

"window.zoomLevel": 1,

// "terminal.integrated.defaultProfile.windows": "Command Prompt"

}
```
### Local setting для проекту Django
В корені проекту створюємо каталог `.vscode` в ньому файл `settings.json` такого вигляду
```JSON
{

"git.autoRepositoryDetection": "subFolders",

"breadcrumbs.enabled": true,

  

"python.languageServer": "Pylance",

"python.analysis.typeCheckingMode": "off",

"python.analysis.diagnosticMode": "openFilesOnly",

"python.analysis.autoSearchPaths": true,

"python.analysis.autoImportCompletions": true,

"python.analysis.completeFunctionParens": true,

"python.analysis.inlayHints.variableTypes": true,

"python.analysis.inlayHints.functionReturnTypes": true,

"python.analysis.importFormat": "absolute",

"python.analysis.enablePytestSupport": true,

"python.analysis.indexing": true,

"python.analysis.packageIndexDepths": [

{

"name": "django",

"depth": 3,

"includeAllSymbols": true

}

],

"[python]": {

"editor.defaultFormatter": "ms-python.black-formatter"

},

"emmet.includeLanguages": {

"django-html": "html"

},

"files.associations": {

"**/*.html": "html",

"**/templates/*/*.html": "django-html",

"**/templates/*/*/*.html": "django-html",

"**/templates/*": "django-txt",

"**/requirements{/**,*}.{txt,in}": "pip-requirements"

},

"django.snippets.exclude": [

"cms",

"wagtail"

],

  
  

"[django-html]": {

"editor.defaultFormatter": "batisteo.vscode-django",

"breadcrumbs.showClasses": true,

"editor.quickSuggestions": {

"other": true,

"comments": true,

"strings": true

}

}

}
```

### Виключення для лінтера

Також потрібно в корені проекту створити файл `.pylintrc` для списку помилок які має ігнорувати лінтер. 
```text
[MESSAGES CONTROL]
disable = missing-function-docstring,
		  missing-module-docstring,
		  missing-class-docstring,
		  import-outside-toplevel,
		  attribute-defined-outside-init,
		  line-too-long,
		  invalid-str-returned,
		  no-member
```

### Необхідні розширення VScode
- Python (Microsoft)
- Pylance (Microsoft)
- Pylint (Microsoft)
- Black Formatter (Microsoft)
- Django (Baptiste Darthenay)
- CSS Peek (Pranay Prakash)
- Auto Rename Tag (Jun Han)
- IntelliSense for CSS class names in HT (Zignd)
- Live Server (Ritwick Dey)

### Налаштування автоматичного запуску Django через файл launch.json
Для автоматичного створення файлу `launch.json` для Django або інших фреймворків потрібно на боковій панелі натиснути Run and Debug  і вибрати "create a launch.json file", і далі вибрати потрібний фреймворк.
### Посилання
[[Django]], [[VSCode]], [[JSON]]
[Відео](https://www.youtube.com/watch?v=e15PvHRHB_w)